////////////////////////////////////////////////////////////
// File: Player.h
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A header file for the player class
////////////////////////////////////////////////////////////

#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "SpaceInvaders.h"
#include "Vector2.h"
#include "Bullet.h"

#include <vector>

class Player
{
public:
	// Function Prototypes
	Player(float a_fPlayerPosX, float a_fPlayerPosY, bool a_bDraw = true);
	Player(float a_fPlayerPosX, float a_fPlayerPosY, SpaceInvaders::GameDifficulty a_GameDifficulty, bool a_bDraw = false);
	~Player();

	Player operator = (Player& a_Player);

	float GetPosX();
	float GetPosY();
	int GetSpriteID();
	short int GetLives();
	float GetWidth();
	float GetHeight();

	unsigned int GetMoveLeftKey();
	unsigned int GetMoveRightKey();

	void draw();
	void update(std::vector<Bullet*> &a_vrPlayerBulletVector);
	void FireBullet(std::vector<Bullet*> &a_vrPlayerBulletVector);
	void SetLives(short int a_siLives);
	void SetPos(float a_fX, float a_fY);
	void ResetPlayer();

	bool bDelete;

protected:

private:
	// Private class member variables
	const float c_fPlayerWidth;
	const float c_fPlayerHeight;
	int iSpriteID;
	float fPlayerMoveSpeed;

	int iScreenWidth;
	int iScreenHeight;

	short int siLives;

	int iMaxBullets;

	unsigned int uiPlayerMoveLeftKey;
	unsigned int uiPlayerMoveRightKey;
	unsigned int uiPlayerShootKey;
	
	Vector2 Position;
};

inline Player Player::operator = (Player& a_Player)
{
	iSpriteID = a_Player.iSpriteID;
	Position.x = a_Player.Position.x;
	Position.y = a_Player.Position.y;
	fPlayerMoveSpeed = a_Player.fPlayerMoveSpeed;
	iScreenHeight = a_Player.iScreenHeight;
	iScreenWidth = a_Player.iScreenWidth;
	siLives = a_Player.siLives;
	iMaxBullets = a_Player.iMaxBullets;
	uiPlayerMoveLeftKey = a_Player.uiPlayerMoveLeftKey;
	uiPlayerMoveRightKey = a_Player.uiPlayerMoveRightKey;
	uiPlayerShootKey = a_Player.uiPlayerShootKey;
	bDelete = a_Player.bDelete;
	return *this;
}

#endif // _PLAYER_H_