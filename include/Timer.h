////////////////////////////////////////////////////////////
// File: Timer.h
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A header file for the timer class
////////////////////////////////////////////////////////////

#ifndef _TIMER_H_
#define _TIMER_H_

class Timer
{

public:

	Timer(float a_fTicksPerTickRate, float a_fTickRate, float a_fSpeedMultiplyer = 1);

	bool Tick();
	float GetSpeedMultiplier();
	float GetTime();

	void SetSpeedMultiplier(float a_fSpeedMultiplier);
	void ResetTimer();

private:
	bool bTick;
	float fTime;
	float fTimeSinceTick;
	
	float fTicksPerTickRate;
	float fTickRate;
	float fSpeedMultiplier;

};

#endif // _TIMER_H_