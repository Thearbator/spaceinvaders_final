////////////////////////////////////////////////////////////
// File: Shield.h
// Author: Zack Raeburn
// Date Created: 13/12/2016
// Brief: A header file for the shield class
////////////////////////////////////////////////////////////

#ifndef _SHIELD_H_
#define _SHIELD_H_

#include "Vector2.h"

class Shield
{
public:
	int iSpriteID;
	Vector2 Position;
	const float c_fWIDTH;
	const float c_fHEIGHT;
	float fMoveAmount;
	int iHealth;
	bool bDelete;

	Shield(float a_fX, float a_fY);
	Shield(const Shield& a_Shield);
	~Shield();

	Shield& operator = (const Shield& a_Shield);

	void MoveUp();
	void draw();
};

inline Shield& Shield::operator = (const Shield& a_Shield)
{
	iSpriteID = a_Shield.iSpriteID;
	Position.x = a_Shield.Position.x;
	Position.y = a_Shield.Position.y;
	fMoveAmount = a_Shield.fMoveAmount;
	iHealth = a_Shield.iHealth;
	bDelete = a_Shield.bDelete;
	return *this;
}

#endif // _SHIELD_H_