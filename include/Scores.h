////////////////////////////////////////////////////////////
// File: Scores.h
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A header file for the scores class
////////////////////////////////////////////////////////////

#ifndef _SCORES_H_
#define _SCORES_H_

#include "Vector2.h"

class Scores
{
public:
	Vector2 Position;
	float fScore;

	// Constructor
	Scores();
	Scores(const float a_ScoreTextXPos, const float a_fScoreTextYPos, float a_fScore);

	Scores operator= (const Scores& a_Score);
};
#endif // _SCORES_H_