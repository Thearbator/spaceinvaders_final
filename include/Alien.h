////////////////////////////////////////////////////////////
// File: Alien.h
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A header file for the alien class
////////////////////////////////////////////////////////////

#ifndef _ALIEN_H_
#define _ALIEN_H_

#include "Vector2.h"
#include "UGFW.h"
#include "Bullet.h"

#include <vector>

class Alien
{
	// Public section containing constructors, a destructor and some function declarations
public:
	enum AlienType { Points10, Points20, Points30 };

	// default values are for the intro screen Vector2ing
	//Alien(int a_iXPos = (iScreenWidth / 4) + 52, int a_iYPos = ((iScreenHeight / 6) * 5) - (iScreenHeight / 9));
	Alien(float a_fXPos, float a_fYPos, enum AlienType a_AlienType, UG::SColour a_SColour = UG::SColour(0xff, 0xff, 0xff, 0xff)); 
	Alien(const Alien& a_Alien);
	~Alien();

	Alien& operator = (const Alien& a_Alien);

	// Declaring some getters and setters
	float GetPosX();
	float GetPosY();
	float GetSize();
	int GetSpriteID();
	short int SpriteInScreen(float a_fScreenWidth);
	short int GetMoveDirection();
	int GetPoints();

	void DestroyObject();
	void SetPos(float a_fXPos, float a_fYPos);
	void UpdateAlien(std::vector<Bullet*>& a_vrBulletVector, float a_fScreenWidth);
	void MoveDown();
	void SwitchSprite();
	void Shot();
	void FireBullet(std::vector<Bullet*> &a_vrBulletVector, float fScreenWidth);

	const unsigned int c_fMOVEAMOUNTX;
	const unsigned int c_fMOVEAMOUNTY;

private:
	int iSpriteID;
	int iPoints;
	int iAnimFrame;
	short int iMoveDirection;

	float uvCoords[2][4];
	float fBulletFireChance;
	float fBulletFireChanceIncrease;
	const float c_fALIENSIZE;
	
	Vector2 Position;	
};

inline Alien& Alien::operator = (const Alien& a_Alien)
{
	iSpriteID = a_Alien.iSpriteID;
	Position.x = a_Alien.Position.x;
	Position.y = a_Alien.Position.y;
	iMoveDirection = a_Alien.iMoveDirection;
	iPoints = a_Alien.iPoints;
	iAnimFrame = a_Alien.iAnimFrame;
	fBulletFireChance = a_Alien.fBulletFireChance;
	fBulletFireChanceIncrease = a_Alien.fBulletFireChanceIncrease;
	uvCoords[0][0] = a_Alien.uvCoords[0][0];  uvCoords[0][1] = a_Alien.uvCoords[0][1]; uvCoords[0][2] = a_Alien.uvCoords[0][2]; uvCoords[0][3] = a_Alien.uvCoords[0][3];
	uvCoords[1][0] = a_Alien.uvCoords[1][0];  uvCoords[1][1] = a_Alien.uvCoords[1][1]; uvCoords[1][2] = a_Alien.uvCoords[1][2]; uvCoords[1][3] = a_Alien.uvCoords[1][3];
	return *this;
}

#endif // _ALIEN_H_
