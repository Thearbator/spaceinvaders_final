////////////////////////////////////////////////////////////
// File: StarField.h
// Author: Zack Raeburn
// Date Created: 13/12/2016
// Brief: This file holds the code for the star field class definition
////////////////////////////////////////////////////////////

#ifndef _STARFIELD_H_
#define _STARFIELD_H_

#include "Vector2.h"

#define MAX_STARS 50

class StarField
{

public:
	StarField(float a_fScreenWidth, float a_fScreenHeight);

	void Update();

	Vector2 posForeground[MAX_STARS];
	Vector2 posMidground[MAX_STARS];
	Vector2 posBackground[MAX_STARS];
	int iScreenWidth;
	int iScreenHeight;
	int iStarMoveSpeed;
	int iForegroundSize;
	int iMidgroundSize;
	int iBackgroundSize;
};

#endif // _STARFIELD_H_