////////////////////////////////////////////////////////////
// File: TitleText.h
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A header file for the title text class
////////////////////////////////////////////////////////////

#ifndef _TITLETEXT_H_
#define _TITLETEXT_H_

#include "UGFW.h"
#include "Vector2.h"

#include <string>

// class to hold the text needed to render the intro screen, these will then later be put into a pointer array to be passed into a function
class TitleText
{
public:
	Vector2 Position;
	std::string sText;
	bool bDraw;
	UG::SColour colour;

	// class constructor, the colour paramet has a default value of black
	TitleText(std::string a_sText, float a_fXPos, float a_fYPos, bool a_bDraw = true, UG::SColour a_Colour = UG::SColour(0xff, 0xff, 0xff, 0xff));

protected:

private:

};
#endif // _TITLETEXT_H_