////////////////////////////////////////////////////////////
// File: Entity.h
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A header file for the main source file
////////////////////////////////////////////////////////////

#ifndef _MAIN_H_
#define _MAIN_H_

#include "UGFW.h"
#include "Enumerations.h"

#include "SpaceInvaders.h"
#include "Alien.h"
#include "Bullet.h"
#include "Player.h"
#include "Scores.h"
#include "TitleText.h"
#include "Vector2.h"
#include "MotherShip.h"
#include "Timer.h"

// Determines how quickly game code is executed
// If fGameSpeed is 1 and fTickRate is also 1 then the function tick() will execute a tick roughly every 1 second
// Therefore at the moment the game ticks roughly 10 times a second
float fGameSpeed;
float fTickRate;

// I'm going to use this variable to slowly increase the alien speed
//initially it should be the same as everything else and then speed up so I'm making it the same as c_fGAMESPEED for now
float fAlienSpeed = fGameSpeed;

// Bool to determine whether the intro sprites have been drawn, without this the game would crash on launch
bool bAreIntroSpritesDrawn;

// Font size for text
float fFontSize;

// Enum for game states
enum GameState { Start, Menu, HighScores, Game, DifficultySelect, GameOver };
enum GameMode { _1Player, _2Player };
enum GameDifficulty { Easy, Medium, Hard };

/*
void tick(bool& a_rbTick, float& a_rfTime, float& a_rfTimeSinceTick, float a_fSpeedMultiplyer = 1.f);
void gameStartSplashScreen(GameState& a_riPGameState, const float& rc_fGAMESPEED, const float& rc_fTICKRATE, bool& rbTick, float& rfTime, float& rfTimeSinceTick, Scores& sPlayer1Score, Scores& sHiScore, Scores& sPlayer2Score);
void drawPlayerScore(Scores* a_PsPlayer1Score, Scores* a_PsHiScore, Scores* a_PsPlayer2Score, bool a_bDrawP1Score = true, bool bDrawHiScore = true, bool bDrawP2Score = true);
void drawVariableToScreen(float a_fVariableToText, float a_fXPos, float a_fYPos, float a_fFontSize, UG::SColour a_UGSColour, char a_cFillLetter = ' ', short int a_siFillAmount = 0);
void drawStringToScreen(std::string a_sVariableToText, float a_fXPos, float a_fYPos, float a_fFontSize, UG::SColour a_UGSColour);
void gameMenuScreen(GameState& a_riGameState, GameMode& a_riGameMode, Scores& sPlayer1Score, Scores& sHiScore, Scores& sPlayer2Score, bool& rbTick, float& rfTime, float& rfTimeSinceTick);
void gameDifficultySelect(GameDifficulty a_riGameDifficulty, GameState& a_riGameState, Scores& sPlayer1Score, Scores& sHiScore, Scores& sPlayer2Score, bool& rbTick, float& rfTime, float& rfTimeSinceTick);
void playGame(GameState& a_riGameState, GameMode& a_riGameMode, Scores& sPlayer1Score, Scores& sHiScore, Scores& sPlayer2Score, bool& rbTick, float& rfTime, float& rfTimeSinceTick);
void gameHighScoreScreen(GameState& a_riGameState, Scores& sPlayer1Score, Scores& sHiScore, Scores& sPlayer2Score);
void textCutScene(std::string a_sCutSceneText, int a_iBufferTime, int a_iCutSceneDuration, bool& rbTick, float& rfTime, float& rfTimeSinceTick, Scores& sPlayer1Score, Scores& sHiScore, Scores& sPlayer2Score, bool a_bDrawP1Score = true, bool a_bDrawHiScore = true, bool a_bDrawP2Score = true);
*/
#endif // _MAIN_H_