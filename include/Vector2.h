////////////////////////////////////////////////////////////
// File: Vector2.h
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A header file for the vector2 class
////////////////////////////////////////////////////////////

#ifndef _VECTOR2_H_
#define _VECTOR2_H_

class Vector2
{
public:
	// Constructors and a destructor
	Vector2();
	Vector2(float a_x, float a_y);
	Vector2(float* a_fv2);
	~Vector2();

	// Casting operators so Entity.cpp doesnt throw an error when I try to make sprites
	operator float*();
	operator const float*() const;

	// Function that returns the values stored in a Vector2 object
	void SetValue(float a_x, float a_y);
	void SetValue(const Vector2& a_v2);

	// Overloading the operators + and += so that I may add together a Vector2 object with a scalar or another Vector2 object
	Vector2 operator + (float a_fScalar);
	Vector2 operator + (const Vector2& a_v2);
	Vector2& operator += (const Vector2& a_v2);

	// The values that can be stored inside a Vector2 object
	union
	{
		// This struct allows the class to store x and y coordinates
		struct
		{
			float x;
			float y;
		};
		struct
		{
			float i[2];
		};
	};

protected:

private:
	
};

// Casting operators
inline Vector2::operator float*()
{
	return static_cast<float*>(i);
}

inline Vector2::operator const float*() const
{
	return static_cast<const float*>(i);
}

// These inline methods are declared here to save space/increase performance I believe
// Defining how overloading of the + operator works on this class with scalars
inline Vector2 Vector2::operator + (float a_fScalar)
{
	return Vector2(x + a_fScalar, y + a_fScalar);
}
// Defining how overloading of the + operator works on this class with other Vector2 objects
inline Vector2 Vector2::operator + (const Vector2& a_v2)
{
	return Vector2(x + a_v2.x, y + a_v2.y);
}
// Defining how overloading of the += operator works on this class with other Vector2 objects
inline Vector2& Vector2::operator += (const Vector2& a_v2)
{
	x += a_v2.x;
	y += a_v2.y;
	return *this;
}

#endif // _VECTOR2_H_
