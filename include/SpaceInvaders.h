////////////////////////////////////////////////////////////
// File: SpaceInvaders.h
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A header file for the Space Invaders class that I will use the run and control the game
////////////////////////////////////////////////////////////

#ifndef _SPACEINVADERS_H_
#define _SPACEINVADERS_H_

#include "Scores.h"
#include "Alien.h"

#include "UGFW.h"

#include <string>
#include <vector>

class SpaceInvaders
{
	// Making the Player and Alien classes a friend so it can access the game difficulty
	friend class Player;
	friend class Alien;
	// Allowing Bullet to use the fScreenWidth and fScreenHeight variables
	friend class Bullet;

private:

	// Determines how quickly game code is executed
	// If fGameSpeed is 1 and fTickRate is also 1 then the function tick() will execute a tick roughly every 1 second
	// Therefore at the moment the game ticks roughly 10 times a second
	float fTicksPerTickRate;
	float fTickRate;

	// These variables will help control the refresh rate of some of the code later on
	// I need 2 variables for this as the player will need to be at a constant rate but the aliens will need to speed up
	float fTime;
	float fTimeSinceTick;

	// Returns true when the game needs to update
	bool bTick;

	// Bool to determine whether the intro sprites have been drawn, without this the game would crash on launch
	bool bAreIntroSpritesDrawn;

	// Font size for text
	float fFontSize;

	// Controls how many bullets aliens can fire
	int iAlienMaxBullets;

	Scores sPlayer1Score;
	Scores sPlayer2Score;
	Scores sHiScore;

	char* cpScoreFilePath;

	const int c_iAMOUNTOFHIGHSCORES;

	const float c_fSWITCHSCREENSPAUSETIME;

protected:
	// Enum for game states
	enum GameState { Start, Menu, HighScores, Game, DifficultySelect, GameOver, Win}iGameState;

	// Enum for game modes
	enum GameMode { _1Player, _2Player }iGameMode;

	// Enum for game difficulties 
	enum GameDifficulty { Easy, Medium, Hard }iGameDifficulty;

public:
	// Numbers that control the window size and Vector2
	float fScreenWidth_SI, fScreenHeight_SI;

	SpaceInvaders(float a_fScreenWidth, float a_fScreenHeight);

	void GameStartSplashScreen();
	void GameMenuScreen();
	void GameDifficultySelect();
	void PlayGame();
	void GameHighScoreScreen();
	void GameOverScreen();
	void WinScreen();

	void AddLife(Player* a_pPlayer, std::vector<Player>& a_vPlayerLifeDrawVector, float a_fLifeSpacing);
	void RemoveLife(Player* a_pPlayer, std::vector<Player>& a_vPlayerLifeDrawVector);

	void DrawPlayerScore(bool a_bDrawP1Score = true, bool bDrawHiScore = true, bool bDrawP2Score = true);
	void DrawVariableToScreen(float a_fVariableToText, float a_fXPos, float a_fYPos, float a_fFontSize, UG::SColour a_UGSColour = UG::SColour(0xff, 0xff, 0xff, 0xff), char a_cFillLetter = ' ', short int a_siFillAmount = 0);
	void DrawStringToScreen(std::string a_sVariableToText, float a_fXPos, float a_fYPos, float a_fFontSize, UG::SColour a_UGSColour = UG::SColour(0xff, 0xff, 0xff, 0xff));

	void TextCutScene(std::string a_sCutSceneText, int a_iBufferTime, int a_iCutSceneDuration, bool a_bDrawP1Score = true, bool a_bDrawHiScore = true, bool a_bDrawP2Score = true);
	void Pause(float fPauseLength);

	void GetScores(int* a_piScoreArray);
	void SetScores(int a_iScore);

	GameState GetGameState();
	GameMode GetGameMode();
	GameDifficulty GetGameDifficulty();
};

#endif //_SPACEINVADERS_H_
