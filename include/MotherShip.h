////////////////////////////////////////////////////////////
// File: Mothership.h
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A header file for the mothership class
////////////////////////////////////////////////////////////

#ifndef _MOTHERSHIP_H_
#define _MOTHERSHIP_H_

#include "Vector2.h"
#include "UGFW.h"

class Mothership
{
public:
	Mothership(float a_fXPos, float a_fYPos, UG::SColour a_SColour = UG::SColour(0xff, 0, 0, 0xff), int a_iMoveDirection = 0);
	Mothership(const Mothership& a_Mothership);
	~Mothership();

	Mothership& operator = (const Mothership& a_Mothership);

	void draw();
	void update();

	float GetPosX();
	float GetPosY();

	const float c_fWIDTH;
	const float c_fHEIGHT;

	int GetSpriteID();


	int iMoveDirection;
	int iMoveAmount;
	int iPoints;

private:
	int iSpriteID;
	Vector2 Position;
};

inline Mothership& Mothership::operator = (const Mothership& a_Mothership)
{
	iSpriteID = a_Mothership.iSpriteID;
	Position.x = a_Mothership.Position.x;
	Position.y = a_Mothership.Position.y;
	iMoveAmount = a_Mothership.iMoveAmount;
	iMoveDirection = a_Mothership.iMoveDirection;
	return *this;
}

#endif // _MOTHERSHIP_H_
