////////////////////////////////////////////////////////////
// File: Bullet.h
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the bullet class
////////////////////////////////////////////////////////////

#ifndef _BULLET_H_
#define _BULLET_H_

#include "Vector2.h"

class Bullet
{
public:
	enum BulletType { Alien = 1, Player = 2 }thisBulletType;

	// Function Prototypes
	Bullet(float a_fPosX, float a_fPosY, const float a_c_fSCREENHEIGHT, enum BulletType a_ThisBulletType = Player, bool a_bDelete = false);
	~Bullet();

	Bullet operator = (Bullet& a_Bullet);

	int GetSpriteID();
	bool SpriteOnScreen();
	bool bDelete;
	float GetWidth();
	float GetHeight();
	float GetPosX();
	float GetPosY();

	void UpdateBullet();
	void draw();

private:
	float fBulletSpeed;
	const float c_fSCREENHEIGHT;
	const float c_fBULLETWIDTH;
	const float c_fBULLETHEIGHT;
	int iSpriteID;
	Vector2 Position;
};

inline Bullet Bullet::operator = (Bullet& a_Bullet)
{
	iSpriteID = a_Bullet.iSpriteID;
	Position.x = a_Bullet.Position.x;
	Position.y = a_Bullet.Position.y;
	fBulletSpeed = a_Bullet.fBulletSpeed;
	thisBulletType = a_Bullet.thisBulletType;
	bDelete = a_Bullet.bDelete;
	return *this;
}

#endif // _BULLET_H_
