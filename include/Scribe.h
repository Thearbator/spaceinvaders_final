#ifndef _SCRIBE_H_
#define _SCRIBE_H_

#include <fstream>
#include <vector>
#include <string>


class Scribe
{

public:
	Scribe();
	void createDataFile(const char* binPath, const char* dirPath);
	void getFileNames(std::vector<std::string>& output, const char* path);
	void extractFile(const char* fileName, const char* searcj, const char* extractPath);
};

#endif // _SCRIBE_H_