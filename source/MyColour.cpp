#include "MyColour.h"

MyColour::MyColour(unsigned short int a_uiRed, unsigned short int a_uiGreen, unsigned short int a_uiBlue, unsigned short int a_uiAlpha)
{
	uiRed = a_uiRed;
	uiGreen = a_uiGreen;
	uiBlue = a_uiBlue;
	uiAlpha = a_uiAlpha;
}

MyColour::~MyColour()
{

}