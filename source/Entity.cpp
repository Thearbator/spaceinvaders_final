////////////////////////////////////////////////////////////
// File: Entity.cpp
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the Entity class
////////////////////////////////////////////////////////////

#include "Entity.h"
#include "UGFW.h"

Entity::Entity(const char* a_textureFileName, float a_width, float a_height)
{
	uiSpriteID = UG::CreateSprite(a_textureFileName, a_width, a_height, true);
}

Entity::Entity(const char* a_textureFileName, const Vector2& a_v2Size, const float* a_fv4UVCoords)
{
	uiSpriteID = UG::CreateSprite(a_textureFileName, a_v2Size, Vector2(0.5f, 0.5f), a_fv4UVCoords);
}

Entity::Entity(const Entity& a_Entity)
{
	uiSpriteID = a_Entity.uiSpriteID;
	fWidth = a_Entity.fWidth;
	fHeight = a_Entity.fHeight;
	Position.SetValue(a_Entity.Position);
}

Entity::~Entity()
{
	UG::DestroySprite(uiSpriteID);
}

void Entity::SetPosition(const Vector2& a_v2Position)
{
	Position.SetValue(a_v2Position);
	UG::MoveSprite(uiSpriteID, &a_v2Position.x);
}

void Entity::GetPosition(Vector2& a_v2Position) const
{
	a_v2Position = Position;
}