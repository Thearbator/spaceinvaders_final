////////////////////////////////////////////////////////////
// File: timer.cpp
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the timer class
////////////////////////////////////////////////////////////

#include "Timer.h"
#include "UGFW.h"

Timer::Timer(float a_fTicksPerTickRate, float a_fTickRate, float a_fSpeedMultiplyer)
{
	fTicksPerTickRate = a_fTicksPerTickRate;
	fTickRate = a_fTickRate;
	fSpeedMultiplier = a_fSpeedMultiplyer;

	bTick = false;
	fTime = 0;
	fTimeSinceTick = 0;

}

bool Timer::Tick()
{
	bTick = false;
	// This code will control how many times a second the game will tick
	fTimeSinceTick += (UG::GetDeltaTime() * (fTicksPerTickRate * fSpeedMultiplier));
	if (fTimeSinceTick > fTickRate)
	{
		fTimeSinceTick = 0;
		bTick = true;
		fTime++;
	}

	return bTick;
}

float Timer::GetSpeedMultiplier()
{
	return fSpeedMultiplier;
}

float Timer::GetTime()
{
	return fTime;
}

void Timer::SetSpeedMultiplier(float a_fSpeedMultiplier)
{
	fSpeedMultiplier = a_fSpeedMultiplier;
}

void Timer::ResetTimer()
{
	bTick = false;
	fTime = 0;
	fTimeSinceTick = 0;
}