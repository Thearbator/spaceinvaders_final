////////////////////////////////////////////////////////////
// File: Mothership.cpp
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the Mothership class
////////////////////////////////////////////////////////////

#include "MotherShip.h"

#include <random>

Mothership::Mothership(float a_fXPos, float a_fYPos, UG::SColour a_SColour, int a_iMoveDirection) :c_fWIDTH(48), c_fHEIGHT(22)
{
	iSpriteID = UG::CreateSprite("./images/Mothership/Mothership.png", c_fWIDTH, c_fHEIGHT, true, a_SColour);
	Position.x = a_fXPos;
	Position.y = a_fYPos;

	iMoveDirection = a_iMoveDirection;
	iMoveAmount = 2;

	//randomised points

	srand(static_cast<int>(time_t(NULL)));
	srand(rand());

	int *iRando = new int(rand() % 4);

	if ((*iRando) == 0)
	{
		iPoints = 50;
	}
	else if ((*iRando) == 1)
	{
		iPoints = 100;
	}
	else if ((*iRando) == 2)
	{
		iPoints = 150;
	}
	else
	{
		iPoints = 300;
	}

	delete iRando;

}

Mothership::Mothership(const Mothership& a_Mothership) : c_fWIDTH(a_Mothership.c_fWIDTH), c_fHEIGHT(a_Mothership.c_fHEIGHT)
{
	iSpriteID = a_Mothership.iSpriteID;
	Position.x = a_Mothership.Position.x;
	Position.y = a_Mothership.Position.y;
	iMoveAmount = a_Mothership.iMoveAmount;
	iMoveDirection = a_Mothership.iMoveDirection;
}

Mothership::~Mothership()
{
	UG::StopDrawingSprite(GetSpriteID());
	UG::DestroySprite(iSpriteID);
}

int Mothership::GetSpriteID()
{
	return iSpriteID;
}
float Mothership::GetPosX()
{
	return Position.x;
}

float Mothership::GetPosY()
{
	return Position.y;
}

void Mothership::draw()
{
	UG::DrawSprite(GetSpriteID());
}

void Mothership::update()
{
	Position.x += (float)(iMoveAmount * iMoveDirection);
	UG::MoveSprite(iSpriteID, Position.x, Position.y);
}