////////////////////////////////////////////////////////////
// File: SpaceInvaders.cpp
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the Space Invaders class, this is where all the main program code will go
////////////////////////////////////////////////////////////

#include "SpaceInvaders.h"

#include "Alien.h"
#include "TitleText.h"
#include "MotherShip.h"
#include "Scores.h"
#include "Bullet.h"
#include "Timer.h"
#include "Player.h"
#include "StarField.h"
#include "Shield.h"

#include "UGFW.h"
#include "Enumerations.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <random>
#include <fstream>
#include <algorithm>

SpaceInvaders::SpaceInvaders(float a_fScreenWidth, float a_fScreenHeight)
	: sPlayer1Score(a_fScreenWidth / 20, a_fScreenHeight, 0),
	sPlayer2Score((a_fScreenWidth / 20) * 14, a_fScreenHeight, 0),
	sHiScore((a_fScreenWidth / 2) - (a_fScreenWidth / 9), a_fScreenHeight, 0),
	c_iAMOUNTOFHIGHSCORES(10),
	c_fSWITCHSCREENSPAUSETIME(4)
{
	fScreenWidth_SI = a_fScreenWidth, fScreenHeight_SI = a_fScreenHeight;

	bAreIntroSpritesDrawn = false;

	fFontSize = 1.5;

	// Initialising the game state enum
	iGameState = Start;
	iGameMode = _1Player;
	iGameDifficulty = Easy;

	// Declaring and initialising Score objects so that I can hold and display scores
	bTick = false;
	fTime = 0;
	fTimeSinceTick = 0;

	fTicksPerTickRate = 10;
	fTickRate = 1;

	cpScoreFilePath = "./Data/Scores.bin";

	std::cout << Bullet::BulletType::Player << std::endl;
	std::cout << Bullet::BulletType::Alien << std::endl;

}



void SpaceInvaders::GameStartSplashScreen()
{
	Timer tMainGameTimer(fTicksPerTickRate, fTickRate);

	// Initialising the intro screen text stuff, Theres got to be a better way of doing this
	// The magic numbers in this are just for Positioning, I didn't think having 20 more variables was worth clearing that up
	TitleText sSplashTitleTheSpaceInvaders("T H E   S P A C E   I N V A D E R S", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 4), (fScreenHeight_SI / 6) * (float)5.5, false);
	TitleText sSplashTitlePresents("P R E S E N T S", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 8), ((fScreenHeight_SI / 6) * 5) + (fScreenHeight_SI / 30), false);
	// No text here yet as I want it to appear gradually later
	TitleText sSplashTitlePart4("", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 7), ((fScreenHeight_SI / 6) * 5) - (fScreenHeight_SI / 25), false);
	TitleText sSplashTitleScoreAdvanceTable("* S C O R E   A D V A N C E   T A B L E *", (fScreenWidth_SI / 2) - (fScreenWidth_SI / (float)3.5), (fScreenHeight_SI / 6) * (float)3.5, false);
	TitleText sSplashTitleMystery("=", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 9), (fScreenHeight_SI / 2), false);
	TitleText sSplashTitleHighAlienPoints("=", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 9), (fScreenHeight_SI / 6) * (float)2.5, false);
	TitleText sSplashTitleMidAlienPoints("=", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 9), (fScreenHeight_SI / 3), false);
	TitleText sSplashTitleLowAlienPoints("=", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 9), (fScreenHeight_SI / 6) * (float)1.5, false, UG::SColour(0, 0xff, 0, 0xff));

	TitleText aPTitleTextArray[] = { sSplashTitleTheSpaceInvaders,  sSplashTitlePresents, sSplashTitlePart4, sSplashTitleScoreAdvanceTable, sSplashTitleMystery, sSplashTitleHighAlienPoints, sSplashTitleMidAlienPoints, sSplashTitleLowAlienPoints };

	int iAmountofTitleTextObjects = 8;

	// A vector of type Alien to hold all the Alien objects that will be created for the title sequence
	std::vector<Alien> vectorForAlien;

	// Variables for the starting Position of the left Alien covering the "Part 4" text on the title screen
	float fAlienLineStartXPos = (fScreenWidth_SI / 4) + 52;
	float fAlienLineStartYPos = ((fScreenHeight_SI / 6) * 5) - (fScreenHeight_SI / 9);

	// This loop will create 12 Alien objects for use on the title screen all next to each other
	for (int iAlienLoopCount = 0; iAlienLoopCount < 12; ++iAlienLoopCount)
	{
		if (iAlienLoopCount != 0)
		{
			Alien sAlien(vectorForAlien[(iAlienLoopCount - 1)].GetPosX() + vectorForAlien[(iAlienLoopCount - 1)].GetSize(), vectorForAlien[(iAlienLoopCount - 1)].GetPosY(), Alien::AlienType::Points30);
			vectorForAlien.push_back(sAlien);
		}
		else
		{
			// The first Alien object created will have the postition of the variables above
			Alien sAlien(fAlienLineStartXPos, fAlienLineStartYPos, Alien::AlienType::Points30);
			vectorForAlien.push_back(sAlien);
		}
	}

	// To offset the sprites X position used in the score advance table
	float fPointsTableSpriteXOffset = (float)(fScreenWidth_SI / 2.75);
	float fPointsTableSpriteyOffset = (float)1.75;
	float fMothershipTableyOffeset = (float)2.75;

	// Creating alien and mother ship objects for the score advance table in the intro
	// Below where they are being created I am positioning them where they need to go
	Alien sAlienPointsTable30(fPointsTableSpriteXOffset, sSplashTitleHighAlienPoints.Position.y, Alien::AlienType::Points30);
	UG::MoveSprite(sAlienPointsTable30.GetSpriteID(), sAlienPointsTable30.GetPosX(), sAlienPointsTable30.GetPosY() - (sAlienPointsTable30.GetSize()*fPointsTableSpriteyOffset));

	Alien sAlienPointsTable20(fPointsTableSpriteXOffset, sSplashTitleMidAlienPoints.Position.y, Alien::AlienType::Points20);
	UG::MoveSprite(sAlienPointsTable20.GetSpriteID(), sAlienPointsTable20.GetPosX(), sAlienPointsTable20.GetPosY() - (sAlienPointsTable20.GetSize()*fPointsTableSpriteyOffset));

	Alien sAlienPointsTable10(fPointsTableSpriteXOffset, sSplashTitleLowAlienPoints.Position.y, Alien::AlienType::Points10, UG::SColour(0, 0xff, 0, 0xff));
	UG::MoveSprite(sAlienPointsTable10.GetSpriteID(), sAlienPointsTable10.GetPosX(), sAlienPointsTable10.GetPosY() - (sAlienPointsTable10.GetSize()*fPointsTableSpriteyOffset));

	Mothership sIntroMothership(fPointsTableSpriteXOffset, sSplashTitleMystery.Position.y, UG::SColour(0xff, 0xff, 0xff, 0xff));
	UG::MoveSprite(sIntroMothership.GetSpriteID(), sIntroMothership.GetPosX(), sIntroMothership.GetPosY() - sIntroMothership.c_fHEIGHT*fMothershipTableyOffeset);

	// Magic numbers are used here as I can't pass variables into a switch statement
	// These numbers are only the timing for the intro sequence
	// The game speed variable decides what equals a second here, if the game speed = 10 then a 10 here is one second
#pragma region introCutScene

	do {
		UG::ClearScreen();

		// Draws the time to the screen, Uncomment this to see the timings in the intro window
		/*
		drawVariableToScreen(fTime, 100, 100, fFontSize, UG::SColour(0xff, 0xff, 0xff, 0xff));
		drawVariableToScreen(fTimeSinceTick, 100, 150, fFontSize, UG::SColour(0xff, 0xff, 0xff, 0xff));
		drawVariableToScreen(bTick, 100, 200, fFontSize, UG::SColour(0xff, 0xff, 0xff, 0xff));
		*/

		// Calls a function that draws the player scores
		DrawPlayerScore();

		if (UG::IsKeyDown(UG::KEY_ENTER))
		{
			iGameState = Menu;

			UG::DestroySprite(sAlienPointsTable30.GetSpriteID());
			UG::DestroySprite(sAlienPointsTable20.GetSpriteID());
			UG::DestroySprite(sAlienPointsTable10.GetSpriteID());
			break;
		}

		// This function takes in variables and then calculates how often to update the game

		if (tMainGameTimer.Tick())
		{

			switch ((int)tMainGameTimer.GetTime())
			{
			case 10:
			{
				aPTitleTextArray[0].bDraw = true;
				aPTitleTextArray[1].bDraw = true;

				for (int iAlienLoopCount = 0; iAlienLoopCount < vectorForAlien.size(); iAlienLoopCount++)
				{
					UG::DrawSprite(vectorForAlien[iAlienLoopCount].GetSpriteID());
					std::cout << vectorForAlien[iAlienLoopCount].GetSpriteID() << std::endl;
				}

				break;
			}
			case 18:
			{
				vectorForAlien[0].DestroyObject();
				break;
			}
			case 19:
			{
				vectorForAlien[1].DestroyObject();
				break;
			}
			case 20:
			{
				vectorForAlien[2].DestroyObject();

				aPTitleTextArray[2].bDraw = true;
				aPTitleTextArray[2].sText.append("P");

				break;
			}
			case 21:
			{
				vectorForAlien[3].DestroyObject();

				aPTitleTextArray[2].sText = "P A";

				break;
			}
			case 22:
			{
				vectorForAlien[4].DestroyObject();

				aPTitleTextArray[2].sText = "P A R";
				break;
			}
			case 23:
			{
				vectorForAlien[5].DestroyObject();

				aPTitleTextArray[2].sText = "P A R T";
				break;
			}
			case 26:
			{
				vectorForAlien[6].DestroyObject();

				aPTitleTextArray[2].sText = "P A R T  F";
				break;
			}
			case 27:
			{
				vectorForAlien[7].DestroyObject();

				aPTitleTextArray[2].sText = "P A R T  F O";
				break;
			}
			case 28:
			{
				vectorForAlien[8].DestroyObject();

				aPTitleTextArray[2].sText = "P A R T  F O  U";
				break;
			}
			case 29:
			{
				vectorForAlien[9].DestroyObject();

				aPTitleTextArray[2].sText = "P A R T  F O  U R";
				break;
			}
			case 31:
			{
				vectorForAlien[10].DestroyObject();
				break;
			}
			case 33:
			{
				vectorForAlien[11].DestroyObject();
				break;
			}
			case 40:
			{
				UG::DrawSprite(sAlienPointsTable10.GetSpriteID());
				UG::DrawSprite(sAlienPointsTable20.GetSpriteID());
				UG::DrawSprite(sAlienPointsTable30.GetSpriteID());
				UG::DrawSprite(sIntroMothership.GetSpriteID());

				aPTitleTextArray[3].bDraw = true;
				aPTitleTextArray[4].bDraw = true;
				aPTitleTextArray[4].sText = "=?";
				break;
			}
			case 41:
			{
				aPTitleTextArray[4].sText = "=?   M";
				break;
			}
			case 42:
			{
				aPTitleTextArray[4].sText = "=?   M Y";
				break;
			}
			case 43:
			{
				aPTitleTextArray[4].sText = "=?   M Y S";
				break;
			}
			case 44:
			{
				aPTitleTextArray[4].sText = "=?   M Y S T";
				break;
			}
			case 45:
			{
				aPTitleTextArray[4].sText = "=?   M Y S T E";
				break;
			}
			case 46:
			{
				aPTitleTextArray[4].sText = "=?   M Y S T E R";
				break;
			}
			case 47:
			{
				aPTitleTextArray[4].sText = "=?   M Y S T E R Y";
				aPTitleTextArray[5].bDraw = true;
				break;
			}
			case 48:
			{
				aPTitleTextArray[5].sText = "=3";
				break;
			}
			case 49:
			{
				aPTitleTextArray[5].sText = "=3 0";
				break;
			}
			case 50:
			{
				aPTitleTextArray[5].sText = "=3 0   P";
				break;
			}
			case 51:
			{
				aPTitleTextArray[5].sText = "=3 0   P O";
				break;
			}
			case 52:
			{
				aPTitleTextArray[5].sText = "=3 0   P O I";
				break;
			}
			case 53:
			{
				aPTitleTextArray[5].sText = "=3 0   P O I N";
				break;
			}
			case 54:
			{
				aPTitleTextArray[5].sText = "=3 0   P O I N T";
				break;
			}
			case 55:
			{
				aPTitleTextArray[5].sText = "=3 0   P O I N T S";
				aPTitleTextArray[6].bDraw = true;
				break;
			}
			case 56:
			{
				aPTitleTextArray[6].sText = "=2";
				break;
			}
			case 57:
			{
				aPTitleTextArray[6].sText = "=2 0";
				break;
			}
			case 58:
			{
				aPTitleTextArray[6].sText = "=2 0   P";
				break;
			}
			case 59:
			{
				aPTitleTextArray[6].sText = "=2 0   P O";
				break;
			}
			case 60:
			{
				aPTitleTextArray[6].sText = "=2 0   P O I";
				break;
			}
			case 61:
			{
				aPTitleTextArray[6].sText = "=2 0   P O I N";
				break;
			}
			case 62:
			{
				aPTitleTextArray[6].sText = "=2 0   P O I N T";
				break;
			}
			case 63:
			{
				aPTitleTextArray[6].sText = "=2 0   P O I N T S";
				aPTitleTextArray[7].bDraw = true;
				break;
			}
			case 64:
			{
				aPTitleTextArray[7].sText = "=1";
				break;
			}
			case 65:
			{
				aPTitleTextArray[7].sText = "=1 0";
				break;
			}
			case 66:
			{
				aPTitleTextArray[7].sText = "=1 0   P";
				break;
			}
			case 67:
			{
				aPTitleTextArray[7].sText = "=1 0   P O";
				break;
			}
			case 68:
			{
				aPTitleTextArray[7].sText = "=1 0   P O I";
				break;
			}
			case 69:
			{
				aPTitleTextArray[7].sText = "=1 0   P O I N";
				break;
			}
			case 70:
			{
				aPTitleTextArray[7].sText = "=1 0   P O I N T";
				break;
			}
			case 71:
			{
				aPTitleTextArray[7].sText = "=1 0   P O I N T S";
				break;
			}
			default:
			{
				if ((int)tMainGameTimer.GetTime() > 85)
				{
					iGameState = Menu;

					UG::DestroySprite(sAlienPointsTable30.GetSpriteID());
					UG::DestroySprite(sAlienPointsTable20.GetSpriteID());
					UG::DestroySprite(sAlienPointsTable10.GetSpriteID());
				}

			}
			}

		}

		// Draws the text in aPTitleTextArray if the member variable bDraw is set to true on the object
		// The loop iterates 8 times because there are 8 objects in aPTitleTextArray
		for (int iLoopCount = 0; iLoopCount < iAmountofTitleTextObjects; ++iLoopCount)
		{
			if (aPTitleTextArray[iLoopCount].bDraw == true)
			{
				DrawStringToScreen(aPTitleTextArray[iLoopCount].sText, aPTitleTextArray[iLoopCount].Position.x, aPTitleTextArray[iLoopCount].Position.y, fFontSize, aPTitleTextArray[iLoopCount].colour);
			}
		}

		// Updates the current frame
		UG::Process();

		// This condition checks that the gamestate is still equal to start
	} while (iGameState == Start);
#pragma endregion
}



void SpaceInvaders::GameMenuScreen()
{
	Pause(c_fSWITCHSCREENSPAUSETIME);

	Timer tMainGameTimer(fTicksPerTickRate, fTickRate);

	// Creating objects of TitleText for all the menu text
	TitleText sSpaceInvaders("S P A C E   I N V A D E R S", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 5), (fScreenHeight_SI / 6) * (float)5.5);
	TitleText s1Player("1   P L A Y E R", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 6), (fScreenHeight_SI / 6) * (float)4.5);
	TitleText s2Player("2   P L A Y E R", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 6), (fScreenHeight_SI / 6) * 4);
	TitleText sHighScores("H I G H   S C O R E S", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 6), (fScreenHeight_SI / 6) * (float)3.5);
	TitleText sExit("E X I T", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 6), (fScreenHeight_SI / 6) * 3);
	TitleText sMenuArrow("<---", (fScreenWidth_SI / 2) + (fScreenWidth_SI / 4), s1Player.Position.y);
	TitleText sMenuInstructions_1("Use the UP and DOWN arrows to navigate", 10, 130);
	TitleText sMenuInstructions_2("ENTER/RETURN to continue", 10, 90);

	// Putting all those objects into a pointer array so I can draw it all later in a loop
	TitleText* aPTitleTextArray[] = { &sSpaceInvaders, &s1Player, &s2Player, &sHighScores, &sExit, &sMenuArrow, &sMenuInstructions_1, &sMenuInstructions_2 };

	// Enum for the menu selection
	enum Selection { _1Player, _2Player, HighScores, Exit };
	Selection mySelection = _1Player;

	// bool for deciding whether the user is in the menu still or not
	bool bInMenu = true;

	// Defining the menu movement keys
	unsigned int kUpKey = UG::KEY_UP;
	unsigned int kDownKey = UG::KEY_DOWN;

	// I'm going to use these bools to make the selection controls a little less precise and frustrating to use as without these it is very hard to navigate through the menu
	// This system could still use some work but for now it seems to be ok
	bool bUpPressed = false;
	bool bDownPressed = false;
	bool bEnterPressed = false;

	do {
		UG::ClearScreen();

		if (UG::IsKeyDown(kUpKey))
		{
			bUpPressed = true;
		}
		if (UG::IsKeyDown(kDownKey))
		{
			bDownPressed = true;
		}
		if (UG::IsKeyDown(UG::KEY_ENTER))
		{
			bEnterPressed = true;
		}

		// If the above function has declared a tick
		if (tMainGameTimer.Tick())
		{
			// Theses cases are fairly similar so I won't repeat comments where they are not needed
			switch (mySelection)
			{
			case _1Player:
			{
				// Setting the Position of the menu arrow to the y Position of s1Player or the 2 object in aPTitleTextArray
				aPTitleTextArray[5]->Position.y = aPTitleTextArray[1]->Position.y;

				// Checking for key inputs so the menu arrow can be moved
				if (bUpPressed)
				{
					mySelection = Exit;
					bUpPressed = false;
				}
				if (bDownPressed)
				{
					mySelection = _2Player;
					bDownPressed = false;
				}

				// If enter is pressed then we need to do something, this is moving onto another gamestate and setting the gamemode
				if (bEnterPressed)
				{
					bInMenu = false;
					iGameState = GameState::DifficultySelect;
					iGameMode = GameMode::_1Player;
					bEnterPressed = false;
				}

				break;
			}
			case _2Player:
			{
				aPTitleTextArray[5]->Position.y = aPTitleTextArray[2]->Position.y;

				if (bUpPressed)
				{
					mySelection = _1Player;
					bUpPressed = false;
				}
				if (bDownPressed)
				{
					mySelection = HighScores;
					bDownPressed = false;
				}

				if (bEnterPressed)
				{
					bInMenu = false;
					iGameState = GameState::DifficultySelect;
					iGameMode = GameMode::_2Player;
					bEnterPressed = false;
				}

				break;
			}
			case HighScores:
			{
				aPTitleTextArray[5]->Position.y = aPTitleTextArray[3]->Position.y;

				if (bUpPressed)
				{
					mySelection = _2Player;
					bUpPressed = false;
				}
				if (bDownPressed)
				{
					mySelection = Exit;
					bDownPressed = false;
				}

				// If enter is pressed whilst High Scores is selected then we will move onto the High Scores screen
				if (bEnterPressed)
				{
					iGameState = GameState::HighScores;
					bInMenu = false;
					bEnterPressed = false;
				}

				break;
			}
			case Exit:
			{
				aPTitleTextArray[5]->Position.y = aPTitleTextArray[4]->Position.y;

				if (UG::IsKeyDown(kUpKey))
				{
					mySelection = HighScores;
					bUpPressed = false;
				}
				if (UG::IsKeyDown(kDownKey))
				{
					mySelection = _1Player;
					bDownPressed = false;
				}

				// Closes the application if enter is pressed
				if (bEnterPressed)
				{
					bInMenu = false;
					bEnterPressed = false;
					UG::Close();
				}
				break;
			}
			// If something goes wrong this will set the selection arrow to _2Player
			default:
			{
				mySelection = _2Player;
				break;
			}
			}
		}

		// Iterates through the array of TitleText objects we created earlier and draws then though a function
		for (int iLoopCount = 0; iLoopCount < std::size(aPTitleTextArray); ++iLoopCount)
		{
			DrawStringToScreen(aPTitleTextArray[iLoopCount]->sText, aPTitleTextArray[iLoopCount]->Position.x, aPTitleTextArray[iLoopCount]->Position.y, fFontSize, aPTitleTextArray[iLoopCount]->colour);
		}

		// Calls a function that draws the player scores
		DrawPlayerScore();

		// Updates the current frame
		UG::Process();

	} while (bInMenu == true);

}



void SpaceInvaders::GameDifficultySelect()
{
	Pause(c_fSWITCHSCREENSPAUSETIME);

	Timer tMainGameTimer(fTicksPerTickRate, fTickRate);

	bool bInScreen = true;

	TitleText sEasy("E A S Y", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 6), (fScreenHeight_SI / 6) * (float)4.5);
	TitleText sMedium("M E D I U M", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 6), (fScreenHeight_SI / 6) * 4);
	TitleText sHard("H A R D", (fScreenWidth_SI / 2) - (fScreenWidth_SI / 6), (fScreenHeight_SI / 6) * (float)3.5);
	TitleText sMenuArrow("<--", sEasy.Position.x + fScreenWidth_SI / 5, sEasy.Position.y);

	TitleText* apTitleTextArray[] = { &sEasy, &sMedium, &sHard, &sMenuArrow };

	// Enum for the menu selection
	enum Selection { Easy, Medium, Hard };
	Selection mySelection = Easy;

	// Defining the menu movement keys
	unsigned int kUpKey = UG::KEY_UP;
	unsigned int kDownKey = UG::KEY_DOWN;
	unsigned int kEnterKey = UG::KEY_ENTER;

	bool bUpPressed;
	bool bDownPressed;
	bool bEnterPressed;

	do
	{
		bUpPressed = false;
		bDownPressed = false;
		bEnterPressed = false;

		if (UG::IsKeyDown(kUpKey))
		{
			bUpPressed = true;
		}
		if (UG::IsKeyDown(kDownKey))
		{
			bDownPressed = true;
		}
		if (UG::IsKeyDown(kEnterKey))
		{
			bEnterPressed = true;
		}

		UG::ClearScreen();

		if (tMainGameTimer.Tick())
		{
			switch (mySelection)
			{
			case Easy:
			{
				apTitleTextArray[3]->Position.y = apTitleTextArray[0]->Position.y;
				if (bUpPressed == true)
					{
						mySelection = Hard;
					}
				else if (bDownPressed == true)
					{
						mySelection = Medium;
					}
				else if (bEnterPressed)
					{
						bInScreen = false;
						iGameDifficulty = GameDifficulty::Easy;
						iGameState = GameState::Game;
					}
				break;
			}
			case Medium:
			{
				apTitleTextArray[3]->Position.y = apTitleTextArray[1]->Position.y;
				if (bUpPressed == true)
					{
						mySelection = Easy;
					}
				else if (bDownPressed == true)
					{
						mySelection = Hard;
					}
				else if (bEnterPressed)
					{
						bInScreen = false;
						iGameDifficulty = GameDifficulty::Medium;
						iGameState = GameState::Game;
					}
				break;
			}
			case Hard:
			{
				apTitleTextArray[3]->Position.y = apTitleTextArray[2]->Position.y;
				if (bUpPressed == true)
					{
						mySelection = Medium;
					}
				else if (bDownPressed == true)
					{
						mySelection = Easy;
					}
				else if (bEnterPressed)
					{
						bInScreen = false;
						iGameDifficulty = GameDifficulty::Hard;
						iGameState = GameState::Game;
					}
				break;
			}
			default:
				{
					std::cout << "Error in gameDifficultySelect Switch" << std::endl;
					break;
				}
			}
		}

		for (int iLoopCount = 0; iLoopCount < std::size(apTitleTextArray); iLoopCount++)
		{
			DrawStringToScreen(apTitleTextArray[iLoopCount]->sText, apTitleTextArray[iLoopCount]->Position.x, apTitleTextArray[iLoopCount]->Position.y, fFontSize, apTitleTextArray[iLoopCount]->colour);
		}

		// Calls a function that draws the player scores
		DrawPlayerScore();

		UG::Process();

	} while (bInScreen);
}



void SpaceInvaders::PlayGame()
{

	// I have spaced clusters of variables that are related, each cluster is separated by 3 new lines
	// Hopefully this makes it easier to read and understand

	Pause(c_fSWITCHSCREENSPAUSETIME);

#pragma region Game variables setup

	// A variable to store what the current game mode is
	short int siPlayerMode;
	// Setting the game mode
	if (iGameMode == GameMode::_1Player)
	{
		siPlayerMode = 1;
	}
	else
	{
		siPlayerMode = 2;
	}



	// Setting how many aliens there will be in the game
	int iAlienColumns = 5;
	int iAlienRows = 11;


	// Mothership variables
	int iMothershipSpawnChance = 250;
	float fMotherShipYPos = fScreenHeight_SI * 0.87f;



	// Setting where the player object will spawn
	float fPlayerStartXPos = fScreenWidth_SI * (float)0.5;
	float fPlayerStartYPos = fScreenHeight_SI / 8;



	// Setting the speeds of the game timers used for the the main game objects and the Aliens
	float fGameUpdateMultiplyer = 4;
	float fAlienSpeedMultiplier = (float)0.03;



	// Setting the top left Alien of the Alien block's position
	// All other Alien positions will be calculated from this
	float fAlienStartXPos = (float)102.4;

	float fAlienXPos;
	float fAlienYPos;

	// Setting the space between Aliens
	// The units are of the Alien Size, so if an Alien is 10 pixels wide then 1 in fAlienSpacing would equal 10 pixels
	float fAlienSpacingX = (float)1.6;
	float fAlienSpacingY = (float)1.2;



	// Variables I will use for bullet collision checking later on, if I don't declare them here then I get warnings
	float fBulletY;
	float fBulletX;
	float fBulletWidthHalf;



	// Making a variable to change the Alien type
	Alien::AlienType aThisAlienType;



	// Bools to control whether to draw player 1 and player 2 score individually
	bool bDrawP1Score = true;
	bool bDrawP2Score = false;



	// Bool to tell whether the game has been setup yet
	bool bSetupGame = true;



	// This bool controls whether the game is running and if a game is in progress
	// IF bGameInProgress is false then the program will check if Player 2 needs a turn and then play again if needs be or break and continue to the main menu
	bool bGameInProgress = true;


	// A float to control how much the speed of the Aliens increases each Alien Tick
	float fAlienSpeedIncrease = (float)0.00005;

	// A bool to control whether the aliens need to move down or not
	bool bAlienMoveDown = false;
	// A bool to memorize whether the Aliens have moved downwards on the last tick
	bool bAlienMovedDown = false;

	

	// Bools for controlling the extra lives given to the player based on score
	bool bExtraLife1Received;
	bool bExtraLife2Received;



	// I will use this float point to either point to Player1 score or Player2 score so that I can add to either in game
	float* pfScorePointer = &sPlayer1Score.fScore;



	// Timers for the game contolling the player and Aliens respectively 
	Timer tMainGameTimer(fTicksPerTickRate, fTickRate, fGameUpdateMultiplyer);
	Timer tAlienTimer(fTicksPerTickRate, fTickRate, fAlienSpeedMultiplier);



	// Creating variables for the positions of the Lives display
	float fLivesObjectsXPos = 50;
	float fLivesObjectsYPos = 100;

	// These variables will be used for the player object's positions that I will use to represent how many lives the has left
	float fObjectXPos = fLivesObjectsXPos * 5;
	float fObjectYPos = fLivesObjectsYPos * (float)0.4;



	// The starting position of the far left Shield 
	float fShieldStartXPos = fScreenWidth_SI * 0.2f;
	float fShieldYPos = fScreenHeight_SI * 0.2f;
	const int c_iAMOUNTOFSHIELDS = 4;



	//Creating a star field for the main game background
	StarField myStarField(fScreenWidth_SI, fScreenHeight_SI);



	// Seeding the random number generator
	srand(static_cast<int>(time(NULL)));
	// I do this twice for better random values
	srand(rand());



#pragma endregion Game variables setup

	/// Start of the game initialisation

#pragma region Vectors
	// Creating a vector to store all the Aliens
	std::vector<Alien*> vAlienGameVector;

	// A vector to hold any bullets the Aliens or the player may fire
	std::vector<Bullet*> vBullets;

	// A vector to store a mothership
	std::vector<Mothership*> vMotherships;

	// A vector to store all the player objects used to represent player lives
	std::vector<Player*> vPlayerLivesObjects;

	// A vector to hold all of the Shields
	std::vector<Shield*> vShieldObjects;
#pragma endregion

	for (int iGameLoop = 0; iGameLoop < 2; iGameLoop++)
	{

		// Creating a player object that the player will control the play the game
		Player myPlayer(fPlayerStartXPos, fPlayerStartYPos, GetGameDifficulty(), false);

		int iPlayerLives = myPlayer.GetLives();

		// Drawing the myPlayer object
		myPlayer.draw();

		// A variable for spacing in between the objects used to represent lives
		float fLivesObjectsSpacing = myPlayer.GetWidth() * (float)1.1;

#pragma region Game Intro
		// Setting up the short intro game cutscene for player 1 and player 2
		if (iGameLoop == 0)
		{
			UG::ClearScreen();
			TextCutScene("P L A Y   P L A Y E R   <  1  >", 2, 6, true, true, false);
		}
		else if (iGameLoop == 1)
		{
			UG::ClearScreen();
			TextCutScene("P L A Y   P L A Y E R   <  2  >", 2, 6, false, true, true);
		}
#pragma endregion

		tMainGameTimer.ResetTimer();
		tAlienTimer.ResetTimer();

		bExtraLife1Received = false;
		bExtraLife2Received = false;

#pragma region Setting up lives
		// Creating the first player object used to represent lives as the player will always start with at least one
		Player *playerLivesObjectFirst = new Player(fObjectXPos, fObjectYPos, false);
		vPlayerLivesObjects.push_back(playerLivesObjectFirst);

		// Creating the rest of the player life objects from the position of the first one
		for (int iPlayerLivesObjectsLoop = 0; iPlayerLivesObjectsLoop < myPlayer.GetLives() - 1; iPlayerLivesObjectsLoop++)
		{
			Player *playerLivesObject = new Player(vPlayerLivesObjects[iPlayerLivesObjectsLoop]->GetPosX() + fLivesObjectsSpacing, fObjectYPos, false);
			vPlayerLivesObjects.push_back(playerLivesObject);
		}

		// Drawing lives
		for (int iDrawLivesLoop = 0; iDrawLivesLoop < vPlayerLivesObjects.size(); iDrawLivesLoop++)
		{
			vPlayerLivesObjects[iDrawLivesLoop]->draw();
		}
#pragma endregion

#pragma region Setting up Shields

		fShieldStartXPos = fScreenWidth_SI * 0.2f;

		for (int iShieldCreation = 0; iShieldCreation < c_iAMOUNTOFSHIELDS; iShieldCreation++)
		{
			Shield *myShield = new Shield(fShieldStartXPos, fShieldYPos);
			vShieldObjects.push_back(myShield);
			fShieldStartXPos += fScreenWidth_SI * 0.2f;
		}	

#pragma endregion


		do {
			
			if (bSetupGame == true)
			{

#pragma region Match Setup
				// Reseting assets
				for (auto iter = vAlienGameVector.begin(); iter != vAlienGameVector.end(); ++iter)
				{
					delete *iter;
				}

				for (auto iter = vBullets.begin(); iter != vBullets.end(); ++iter)
				{
					delete *iter;
				}

				for (auto iter = vMotherships.begin(); iter != vMotherships.end(); ++iter)
				{
					delete *iter;
				}

#pragma region Setting up Aliens
				vMotherships.clear();
				vAlienGameVector.clear();
				vBullets.clear();

				fAlienXPos = fAlienStartXPos;
				fAlienYPos = fScreenHeight_SI * (float)0.8;

				// Set the current Alien Type
				aThisAlienType = Alien::AlienType::Points30;

				// For loop to create columns of Aliens
				for (int iAlienColumnCount = 0; iAlienColumnCount < iAlienColumns; iAlienColumnCount++)
				{
					// For loop to create rows of Aliens
					for (int iAlienRowCount = 0; iAlienRowCount < iAlienRows; iAlienRowCount++)
					{
						// Creating new Aliens
						Alien* alien = new Alien(fAlienXPos, fAlienYPos, aThisAlienType);
						// Putting new Aliens into the alien vector
						vAlienGameVector.push_back(alien);
						// Updating the position at which Aliens will be placed
						fAlienXPos += alien->GetSize() * fAlienSpacingX;
					}

					// Updating Alien spawn location on the Y axis
					fAlienYPos -= vAlienGameVector[iAlienColumnCount * iAlienColumns]->GetSize() * fAlienSpacingY;
					// Resetting the Alien X axis spawn location
					fAlienXPos = fAlienStartXPos;

					// Setting the Alien Types for different rows
					if (iAlienColumnCount == 0)
					{
						aThisAlienType = Alien::AlienType::Points20;
					}
					else if (iAlienColumnCount == 2)
					{
						aThisAlienType = Alien::AlienType::Points10;
					}
				}

				// Drawing all the Aliens that we created earlier
				for (int iDrawAlienLoop = 0; iDrawAlienLoop < vAlienGameVector.size(); iDrawAlienLoop++)
				{
					UG::DrawSprite(vAlienGameVector[iDrawAlienLoop]->GetSpriteID());
				}
#pragma endregion

#pragma endregion

				bSetupGame = false;

			}

#pragma region Main Game Code
			// Clearing the screen
			UG::ClearScreen();

#pragma region Alien Tick
			if (tAlienTimer.Tick())
			{

				// Reset the Alien move down control variable
				bAlienMoveDown = false;

				// Make the aliens either move across or move down and change direction based on every current Alien's X coordinate 
				for (int iAlienCheckLoop = 0; iAlienCheckLoop < vAlienGameVector.size(); iAlienCheckLoop++)
				{
					if (vAlienGameVector[iAlienCheckLoop]->SpriteInScreen(fScreenWidth_SI) == 1 && bAlienMovedDown == false)
					{
						bAlienMoveDown = true;
						break;
					}
					else if (vAlienGameVector[iAlienCheckLoop]->SpriteInScreen(fScreenWidth_SI) == -1 && bAlienMovedDown == false)
					{
						bAlienMoveDown = true;
						break;
					}
				}

				// If the Aliens are queued to move down
				if (bAlienMoveDown == true)
				{
					// Move all the Aliens down by iterating though a For loop
					for (int iUpdateAlienLoopMoveDown = 0; iUpdateAlienLoopMoveDown < vAlienGameVector.size(); iUpdateAlienLoopMoveDown++)
					{
						// Call the Alien.MoveDown function
						vAlienGameVector[iUpdateAlienLoopMoveDown]->MoveDown();
						// Set bAlienMovedDown to true so the program knows the Aliens have just moved down
						bAlienMovedDown = true;
					}
				}
				// If the Aliens are not queued to move down
				else
				{
					for (int iUpdateAlienLoopUpdate = 0; iUpdateAlienLoopUpdate < vAlienGameVector.size(); iUpdateAlienLoopUpdate++)
					{
						// Call the Alien.Update function
						vAlienGameVector[iUpdateAlienLoopUpdate]->UpdateAlien(vBullets, fScreenWidth_SI);
						// Set bAlienMovedDown to true so the program knows the Aliens have just moved down
						bAlienMovedDown = false;
					}
				}

				// Increase the tick speed of the Alien timer to simulate the Aliens speeding up
				tAlienTimer.SetSpeedMultiplier(tAlienTimer.GetSpeedMultiplier() + fAlienSpeedIncrease);
			}
#pragma endregion

#pragma region Main Game Tick

			// Check if the main game timer has ticked
			if (tMainGameTimer.Tick())
			{
				// Update the player object
				myPlayer.update(vBullets);

				// Update all the current bullets
				for (int iBulletUpdateLoop = 0; iBulletUpdateLoop < vBullets.size(); iBulletUpdateLoop++)
				{
					vBullets[iBulletUpdateLoop]->UpdateBullet();
				}

				// Creating and updating motherships
				if (vMotherships.size() == 0)
				{
					// Randomly spawn a mothership based on the value of iMothershipSpawnChance

					if (rand() % iMothershipSpawnChance == 1)
					{
						float fMothershipXPos;
						int iMoveDirection;

						// Also set the move direction and which side of the screen it will come from
						if (rand() % 2 == 0)
						{
							fMothershipXPos = 0 - (fScreenWidth_SI * 0.1f);
							iMoveDirection = 1;
						}
						else
						{
							fMothershipXPos = fScreenWidth_SI * 1.1f;
							iMoveDirection = -1;
						}

						// Add it to an array and draw it
						Mothership *myMotherShip = new Mothership(fMothershipXPos, fMotherShipYPos, UG::SColour(0xff, 0xff, 0xff, 0xff), iMoveDirection);
						vMotherships.push_back(myMotherShip);

						vMotherships[0]->draw();
					}
				// If a mother ship already exists then just update it
				}
				else
				{
					vMotherships[0]->update();
				}

				// If a mother ship exists and its off the screen and past its spawn point then delete it
				if (vMotherships.size() > 0)
				{
					if (vMotherships[0]->GetPosX() >= (fScreenWidth_SI * 1.2f) || vMotherships[0]->GetPosX() <= 0 - (fScreenWidth_SI * 0.2f))
					{
						delete vMotherships[0];
						vMotherships.erase(vMotherships.begin());
					}
				}

				// This inner loop will iterate through all the bullets currently in game and see if they have collided with the Aliens
				for (int iBulletCollisionLoopOuter = 0; iBulletCollisionLoopOuter < vBullets.size(); iBulletCollisionLoopOuter++)
				{
					// Getting the positions of the bullet
					fBulletY = vBullets[iBulletCollisionLoopOuter]->GetPosY() + (vBullets[iBulletCollisionLoopOuter]->GetHeight() * 0.5f);
					fBulletX = vBullets[iBulletCollisionLoopOuter]->GetPosX();
					fBulletWidthHalf = vBullets[iBulletCollisionLoopOuter]->GetWidth() / 2.f;


					if (vBullets[iBulletCollisionLoopOuter]->thisBulletType == Bullet::BulletType::Player)
					{
						// Check for collision between player bullets and Aliens
						// This outer loop will iterate through all the Aliens currently in game
						for (int iBulletAndAlienLoopInner = 0; iBulletAndAlienLoopInner < vAlienGameVector.size(); iBulletAndAlienLoopInner++)
						{
							// Getting half the size of the Alien, since the Aliens are positioned from the center I will need to subtract or add half the size to get the edges
							float fAlienSizeBuffer = (vAlienGameVector[iBulletAndAlienLoopInner]->GetSize() * 0.5f);

							// Getting the positions of the Alien
							float fAlienY1 = vAlienGameVector[iBulletAndAlienLoopInner]->GetPosY() - fAlienSizeBuffer;
							float fAlienY2 = vAlienGameVector[iBulletAndAlienLoopInner]->GetPosY() + fAlienSizeBuffer;

							float fAlienX1 = vAlienGameVector[iBulletAndAlienLoopInner]->GetPosX() - fAlienSizeBuffer;
							float fAlienX2 = vAlienGameVector[iBulletAndAlienLoopInner]->GetPosX() + fAlienSizeBuffer;

							// If the bullet is colliding with the Alien on the X axis
							if ((fBulletX + fBulletWidthHalf) >= fAlienX1 && (fBulletX - fBulletWidthHalf) <= fAlienX2)
							{
								// If the bullet is colliding with the Alien on the Y axis
								if (fBulletY >= fAlienY1 && fBulletY <= fAlienY2)
								{
									// Calls the Alien Shot() function on the current Alien
									vAlienGameVector[iBulletAndAlienLoopInner]->Shot();

									(*pfScorePointer) += vAlienGameVector[iBulletAndAlienLoopInner]->GetPoints();

									// Queuing a bullet to be deleted as if I do it now it would cause an out of range errors
									vBullets[iBulletCollisionLoopOuter]->bDelete = true;

									// Erasing the Alien in question from the Alien vector
									//delete the game object from the vector
									Alien* deadGuy = *(vAlienGameVector.begin() + iBulletAndAlienLoopInner);
									delete deadGuy;

									vAlienGameVector.erase(vAlienGameVector.begin() + iBulletAndAlienLoopInner);

									// Increase the tick speed of the Alien timer to simulate the Aliens speeding up
									tAlienTimer.SetSpeedMultiplier(tAlienTimer.GetSpeedMultiplier() + fAlienSpeedIncrease);
								}
							}
						}

						// Mothership - bullet collision
						if (vMotherships.size() > 0)
						{
							if ((fBulletX - fBulletWidthHalf) >= vMotherships[0]->GetPosX() - (vMotherships[0]->c_fWIDTH / 2) && (fBulletX + fBulletWidthHalf) <= vMotherships[0]->GetPosX() + (vMotherships[0]->c_fWIDTH / 2))
							{
								if (fBulletY >= vMotherships[0]->GetPosY() - (vMotherships[0]->c_fHEIGHT / 2) && fBulletY <= vMotherships[0]->GetPosY() + (vMotherships[0]->c_fHEIGHT / 2))
								{
									(*pfScorePointer) += vMotherships[0]->iPoints;

									delete vMotherships[0];
									vMotherships.erase(vMotherships.begin());
								}

							}
						}
					}

					// If the bullet was fired by an Alien then check collision with player
					if (vBullets[iBulletCollisionLoopOuter]->thisBulletType == Bullet::BulletType::Alien)
					{
						// Check for collision between Alien bullets and the player
						for (int iBulletAndPlayerLoop = 0; iBulletAndPlayerLoop < vBullets.size(); iBulletAndPlayerLoop++)
						{
							// Getting half the size of the Alien, since the Aliens are positioned from the center I will need to subtract or add half the size to get the edges
							float fPlayerWidthHalf = (myPlayer.GetWidth() * 0.5f);
							float fPlayerHeightHalf = (myPlayer.GetHeight() * 0.5f);

							// Getting the positions of the Player
							float fPlayerY1 = myPlayer.GetPosY() - fPlayerHeightHalf;
							float fPlayerY2 = myPlayer.GetPosY() + fPlayerHeightHalf;

							float fPlayerX1 = myPlayer.GetPosX() - fPlayerWidthHalf;
							float fPlayerX2 = myPlayer.GetPosX() + fPlayerWidthHalf;

							// If the bullet is colliding with the Alien on the X axis
							if ((fBulletX - fBulletWidthHalf) >= fPlayerX1 && (fBulletX + fBulletWidthHalf) <= fPlayerX2)
							{
								// If the bullet is colliding with the Alien on the Y axis
								if (fBulletY >= fPlayerY1 && fBulletY <= fPlayerY2)
								{
									vBullets[iBulletCollisionLoopOuter]->bDelete = true;

									// Removing a life and updating the lives display if the player collides with a bullet
									if (myPlayer.GetLives() > 0 || vPlayerLivesObjects.size() > 0)
									{
										short int siLive = myPlayer.GetLives();
										
										vPlayerLivesObjects[siLive - 1]->bDelete = true;

										siLive--;
										myPlayer.SetLives(siLive);
									}
									else
									{
										GameOverScreen();
										bSetupGame = true;
										bGameInProgress = false;
									}
									break;
								}
							}
						}
					}
					
					// Barrier and shield collision
					for (int iBarrierCollision = 0; iBarrierCollision < vShieldObjects.size(); iBarrierCollision++)
					{
						float fShieldHalfWidth = vShieldObjects[iBarrierCollision]->c_fWIDTH / 2;
						float fShieldHalfHeight = vShieldObjects[iBarrierCollision]->c_fHEIGHT / 2;


						float fShieldX1 = vShieldObjects[iBarrierCollision]->Position.x - fShieldHalfWidth;
						float fShieldX2 = vShieldObjects[iBarrierCollision]->Position.x + fShieldHalfWidth;

						float fShieldY1 = vShieldObjects[iBarrierCollision]->Position.y - fShieldHalfHeight;
						float fShieldY2 = vShieldObjects[iBarrierCollision]->Position.y + fShieldHalfHeight;
						
						if (fBulletX >= fShieldX1 && fBulletX <= fShieldX2)
						{
							if (fBulletY >= fShieldY1 && fBulletY <= fShieldY2)
							{
								vBullets[iBulletCollisionLoopOuter]->bDelete = true;

								if (vShieldObjects[iBarrierCollision]->iHealth <= 0)
								{
									vShieldObjects[iBarrierCollision]->bDelete = true;
								}
								else
								{
									vShieldObjects[iBarrierCollision]->iHealth--;
									vShieldObjects[iBarrierCollision]->MoveUp();
								}
							}
						}
					}
				}

				for (int iAlienScreenCollision = 0; iAlienScreenCollision < vAlienGameVector.size(); iAlienScreenCollision++)
				{
					if (vAlienGameVector[iAlienScreenCollision]->GetPosY() < 0)
					{
						// Erasing the Alien in question from the Alien vector
						//delete the game object from the vector
						Alien* deadGuy = *(vAlienGameVector.begin() + iAlienScreenCollision);
						delete deadGuy;

						vAlienGameVector.erase(vAlienGameVector.begin() + iAlienScreenCollision);
					}
				}

				// Delete all the bullets that have been queued for deletion
				for (int iBulletDeleter = 0; iBulletDeleter < vBullets.size(); iBulletDeleter++)
				{
					if (vBullets[iBulletDeleter]->bDelete == true)
					{
						Bullet* bullet = *(vBullets.begin() + iBulletDeleter);
						vBullets.erase(vBullets.begin() + iBulletDeleter);

						delete bullet;
					}
				}

				for (int iBarrierDelete = 0; iBarrierDelete < vShieldObjects.size(); iBarrierDelete++)
				{
					if (vShieldObjects[iBarrierDelete]->bDelete == true)
					{
						Shield* deleteShield = *(vShieldObjects.begin() + iBarrierDelete);
						vShieldObjects.erase(vShieldObjects.begin() + iBarrierDelete);

						delete deleteShield;
					}
				}
				
				for (int iPlayerLifeDelete = 0; iPlayerLifeDelete < vPlayerLivesObjects.size(); iPlayerLifeDelete++)
				{
					if (vPlayerLivesObjects[iPlayerLifeDelete]->bDelete == true)
					{
						Player* deleteLife = *(vPlayerLivesObjects.begin() + iPlayerLifeDelete);
						delete deleteLife;

						vPlayerLivesObjects.erase(vPlayerLivesObjects.begin() + iPlayerLifeDelete);
					}
				}

				// If there are no more Aliens call the win screen the congratulate the player and set a variable that will exit this loop
				if (vAlienGameVector.size() == 0)
				{
					bSetupGame = true;
					WinScreen();
				}

				// Drawing the text "Lives" to the screen to help represent the lives count
				DrawStringToScreen("L I V E S :", fLivesObjectsXPos, fLivesObjectsYPos, fFontSize);

				// Drawing player score
				DrawPlayerScore(bDrawP1Score, true, bDrawP2Score);

				//Updating the star field
				myStarField.Update();

				// Updating the current frame
				UG::Process();
			}

			// Score control since the original games score only went to 9,990
			if ((*pfScorePointer) > 9990)
			{
				(*pfScorePointer) = 9990;
			}

			// The original game also gave 2 extra lives to the player when they hit 1,000 and 1,500 points
			if (bExtraLife1Received == false)
			{
				if ((*pfScorePointer) > 1000)
				{
					myPlayer.SetLives(myPlayer.GetLives() + 1);
					int lifeCount = vPlayerLivesObjects.size();
					Player *playerLivesObject = new Player(vPlayerLivesObjects[lifeCount-1]->GetPosX() + fLivesObjectsSpacing, fObjectYPos, false);
					vPlayerLivesObjects.push_back(playerLivesObject);
					bExtraLife1Received = true;
				}
			}

			if (bExtraLife2Received == false)
			{
				if ((*pfScorePointer) > 1500)
				{
					myPlayer.SetLives(myPlayer.GetLives() + 1);
					int lifeCount = vPlayerLivesObjects.size();
					Player *playerLivesObject = new Player(vPlayerLivesObjects[lifeCount - 1]->GetPosX() + fLivesObjectsSpacing, fObjectYPos, false);
					vPlayerLivesObjects.push_back(playerLivesObject);
					bExtraLife2Received = true;
				}
			}

#pragma endregion

#pragma endregion

		} while (bGameInProgress == true);

		// Saving scores
		SetScores((int)(*pfScorePointer));

		// If we are in 1 player mode then break out of the loop
		if (iGameMode == GameMode::_1Player)
		{
			break;
		}
		// Else reset the game for player 2
		else
		{
			bGameInProgress = true;
			// Change the score pointer so I can edit player 2's score
			pfScorePointer = &sPlayer2Score.fScore;
			bDrawP1Score = false;
			bDrawP2Score = true;

			myPlayer.SetLives((short int)iPlayerLives);
			myPlayer.SetPos(fPlayerStartXPos, fPlayerStartYPos);
		}

	}

	// Deleting assets
	for (auto iter = vAlienGameVector.begin(); iter != vAlienGameVector.end(); ++iter)
	{
		delete *iter;
	}

	for (auto iter = vBullets.begin(); iter != vBullets.end(); ++iter)
	{
		delete *iter;
	}

	for (auto iter = vShieldObjects.begin(); iter != vShieldObjects.end(); ++iter)
	{
		delete *iter;
	}

	for (auto iter = vMotherships.begin(); iter != vMotherships.end(); ++iter)
	{
		delete *iter;
	}

	for (auto iter = vPlayerLivesObjects.begin(); iter != vPlayerLivesObjects.end(); ++iter)
	{
		delete *iter;
	}

	// Clearing the vectors
	vPlayerLivesObjects.clear();
	vMotherships.clear();
	vShieldObjects.clear();
	vAlienGameVector.clear();
	vBullets.clear();

	// Changing the hiscore value at the top of the screen
	if (sPlayer1Score.fScore > sHiScore.fScore)
	{
		sHiScore.fScore = sPlayer1Score.fScore;
	}
	else if (sPlayer2Score.fScore > sHiScore.fScore)
	{
		sHiScore.fScore = sPlayer2Score.fScore;
	}

	// Resetting scores
	sPlayer1Score.fScore = 0.f;
	sPlayer2Score.fScore = 0.f;

	// Changing the game state back to the menu
	iGameState = Menu;

}



void SpaceInvaders::GameHighScoreScreen()
{
	Pause(c_fSWITCHSCREENSPAUSETIME);

	bool bHighScoreScreen = true;

	float fHighScoreXPos = fScreenWidth_SI * 0.35f;
	float fHighScoreYPos = fScreenHeight_SI * 0.2f;
	float fHighScoreTitleYPos = fScreenHeight_SI * 0.9f;

	TitleText tHighScore("H I G H   S C O R E S :", fHighScoreXPos, fHighScoreTitleYPos);

	float fTextSpacing = 50;

	float fScoreNumbersXSpacing = 150;

	int iScoreArray[10];

	GetScores(iScoreArray);

	do
	{
		UG::ClearScreen();

		DrawStringToScreen(tHighScore.sText, tHighScore.Position.x, tHighScore.Position.y, fFontSize);

		for (int iHighScoreTextCount = 0; iHighScoreTextCount < c_iAMOUNTOFHIGHSCORES; iHighScoreTextCount++)
		{
			DrawStringToScreen(std::to_string(-(iHighScoreTextCount - 10)), fHighScoreXPos, fHighScoreYPos + (fTextSpacing * ((float)iHighScoreTextCount + 1)), fFontSize);
			DrawVariableToScreen((float)iScoreArray[iHighScoreTextCount], fHighScoreXPos + fScoreNumbersXSpacing, fHighScoreYPos + (fTextSpacing * ((float)iHighScoreTextCount + 1)), fFontSize);
		}

		// Calls a function that draws the player scores
		DrawPlayerScore();

		UG::Process();

		if (UG::IsKeyDown(UG::KEY_ENTER))
		{
			bHighScoreScreen = false;
		}

	} while (bHighScoreScreen);

	iGameState = Menu;
}



void SpaceInvaders::GameOverScreen()
{
	int iGameOverBufferTimer = 4;
	int iGameOverLength = 12;

	TextCutScene("G A M E   O V E R", iGameOverBufferTimer, iGameOverLength);

	iGameState = Menu;
}



void SpaceInvaders::WinScreen()
{
	bool bInScene = true;
	float fCutSceneStart = 4;
	float fCutScenePart2 = fCutSceneStart * 4;
	float fCutSceneLength = fCutScenePart2 * 1.5f;

	std::string sText1 = "W E L L   D O N E   E A R T H L I N G";
	std::string sText2 = "T H I S   T I M E   Y O U   W I N";

	float fText1XPos = (fScreenWidth_SI / 2) - (sText1.size() * (fFontSize * 4));
	float fText2XPos = (fScreenWidth_SI / 2) - (sText2.size() * (fFontSize * 4));
	float fText1YPos = fScreenHeight_SI / 1.5f;
	float fText2YPos = fScreenHeight_SI / 2;

	Timer winTimer(fTicksPerTickRate, fTickRate);

	do
	{
		winTimer.Tick();

		UG::ClearScreen();

		DrawPlayerScore();

		if (winTimer.GetTime() > fCutSceneLength)
		{
			bInScene = false;
		}
		else if (winTimer.GetTime() > fCutScenePart2)
		{
			DrawStringToScreen(sText1, fText1XPos, fText1YPos, fFontSize);
			DrawStringToScreen(sText2, fText2XPos, fText2YPos, fFontSize);
		}
		else if (winTimer.GetTime() > fCutSceneStart)
		{
			DrawStringToScreen(sText1, fText1XPos, fText1YPos, fFontSize);
		}

		UG::Process();
	} while (bInScene);
}



void SpaceInvaders::AddLife(Player* a_pPlayer, std::vector<Player>& a_vPlayerLifeDrawVector, float a_fLifeSpacing)
{
	/// I tried to get this code to simply add one object to the PlayerLifeDraw vector however every attempt left all the other objects in the vector not drawing
	/// So although this code is long winded it is the only working version I have managed to produce

	// Gets the size of the vector before clearing it
	int iVectorSize = (int)a_vPlayerLifeDrawVector.size();

	// Gets the position of the first element before clearing it
	float fStartXPos = a_vPlayerLifeDrawVector[0].GetPosX();
	float fStartYPos = a_vPlayerLifeDrawVector[0].GetPosY();

	// Clears the vector
	a_vPlayerLifeDrawVector.clear();

	// Creates the same amount of objects that were in the vector before clearing +1
	for (int iAddLifeLoop = 0; iAddLifeLoop < iVectorSize + 1; iAddLifeLoop++)
	{
		Player playerLivesObject(fStartXPos, fStartYPos, false);
		a_vPlayerLifeDrawVector.push_back(playerLivesObject);

		// Adds some spacing between this object and the last on the x axis
		fStartXPos = a_vPlayerLifeDrawVector[iAddLifeLoop].GetPosX() + a_fLifeSpacing;
	}
	
	// Draws all the objects we just created, I have a draw() function in the constructor of the Player class however if I try to call it in the object initialisation then it doesn't draw
	for (int iDrawObjects = 0; iDrawObjects < a_vPlayerLifeDrawVector.size(); iDrawObjects++)
	{
		a_vPlayerLifeDrawVector[iDrawObjects].draw();
	}

	// Increase player lives by 1
	a_pPlayer->SetLives(a_pPlayer->GetLives() + 1);
}



void SpaceInvaders::RemoveLife(Player* a_pPlayer, std::vector<Player>& a_vPlayerLifeDrawVector)
{
	// Removes the last element in the vector
	a_vPlayerLifeDrawVector.pop_back();
	// Reduces player lives by 1
	a_pPlayer->SetLives(a_pPlayer->GetLives() - 1);
}



void SpaceInvaders::DrawPlayerScore(bool a_bDrawP1Score, bool a_bDrawHiScore, bool a_bDrawP2Score)
{
	UG::SetFont("./fonts/invaders.fnt");

	// Displays static text that will always be the same
	UG::DrawString("SCORE <1>", (int)sPlayer1Score.Position.x, (int)sPlayer1Score.Position.y, fFontSize, UG::SColour(0xff, 0xff, 0xff, 0xff));
	UG::DrawString("HI-SCORE", (int)sHiScore.Position.x, (int)sHiScore.Position.y, fFontSize, UG::SColour(0xff, 0xff, 0xff, 0xff));
	UG::DrawString("SCORE <2>", (int)sPlayer2Score.Position.x, (int)sPlayer2Score.Position.y, fFontSize, UG::SColour(0xff, 0xff, 0xff, 0xff));

	// Magic numbers are for Positioning the scores on screen

	if (a_bDrawP1Score)
	{
		// Displaying the score for player 1 then clearing the string stream
		DrawVariableToScreen(sPlayer1Score.fScore, sPlayer1Score.Position.x + (fScreenWidth_SI / 20), sPlayer1Score.Position.y, fFontSize, UG::SColour(0xff, 0xff, 0xff, 0xff), '0', 4);
	}

	if (a_bDrawHiScore)
	{
		// Displaying the score for the high score then clearing the string stream
		DrawVariableToScreen(sHiScore.fScore, sHiScore.Position.x + (fScreenWidth_SI / 20), sHiScore.Position.y, fFontSize, UG::SColour(0xff, 0xff, 0xff, 0xff), '0', 4);
	}

	if (a_bDrawP2Score)
	{
		// Displaying the score for player 2 then clearing the string stream
		DrawVariableToScreen(sPlayer2Score.fScore, sPlayer2Score.Position.x + (fScreenWidth_SI / 20), sPlayer2Score.Position.y, fFontSize, UG::SColour(0xff, 0xff, 0xff, 0xff), '0', 4);
	}
}



void SpaceInvaders::DrawVariableToScreen(float a_fVariableToText, float a_fXPos, float a_fYPos, float a_fFontSize, UG::SColour a_UGSColour, char a_cFillLetter, short int a_siFillAmount)
{
	std::ostringstream ssConverter;

	UG::SetFont("./fonts/invaders.fnt");

	ssConverter << std::setfill(a_cFillLetter) << std::setw(a_siFillAmount) << a_fVariableToText;
	//ssConverter.precision(a_iDecimalPoints);
	UG::DrawString(ssConverter.str().c_str(), (int)a_fXPos, (int)(a_fYPos - (fScreenHeight_SI / 20)), a_fFontSize, a_UGSColour);
	ssConverter.str("");
	ssConverter.clear();
}



void SpaceInvaders::DrawStringToScreen(std::string a_sVariableToText, float a_fXPos, float a_fYPos, float a_fFontSize, UG::SColour a_UGSColour)
{
	std::ostringstream ssConverter;

	UG::SetFont("./fonts/invaders.fnt");

	ssConverter << a_sVariableToText;
	//ssConverter.precision(a_iDecimalPoints);
	UG::DrawString(ssConverter.str().c_str(), (int)a_fXPos, (int)(a_fYPos - (fScreenHeight_SI / 20)), a_fFontSize, a_UGSColour);
	ssConverter.str("");
	ssConverter.clear();
}



void SpaceInvaders::TextCutScene(std::string a_sCutSceneText, int a_iBufferTime, int a_iCutSceneDuration, bool a_bDrawP1Score, bool a_bDrawHiScore, bool a_bDrawP2Score)
{
	Timer tMyTimer(fTicksPerTickRate, fTickRate);

	float fTextXPos = (fScreenWidth_SI / 2) - (a_sCutSceneText.size() * (fFontSize * 4));
	float fTextYPos = fScreenHeight_SI / 2;

	TitleText cutSceneText(a_sCutSceneText, fTextXPos, fTextYPos, false);

	bool bInCutScene = true;
	int iTotalCutSceneDuration = (a_iBufferTime * 2) + a_iCutSceneDuration;

	do {

		UG::ClearScreen();

		tMyTimer.Tick();
		if ((int)tMyTimer.GetTime() > iTotalCutSceneDuration)
		{
			bInCutScene = false;
		}
		else if ((int)tMyTimer.GetTime() > a_iBufferTime + a_iCutSceneDuration)
		{
			cutSceneText.bDraw = false;
		}
		else if ((int)tMyTimer.GetTime() > a_iBufferTime)
		{
			cutSceneText.bDraw = true;
		}

		if ((int)tMyTimer.GetTime() % 4)
		{
			DrawPlayerScore(a_bDrawP1Score, a_bDrawHiScore, a_bDrawP2Score);
		}

		DrawStringToScreen(cutSceneText.sText, cutSceneText.Position.x, cutSceneText.Position.y, fFontSize, UG::SColour(0xff, 0xff, 0xff, 0xff));

		UG::Process();

	} while (bInCutScene);

}



void SpaceInvaders::Pause(float fPauseLength)
{
	Timer *myTimer = new Timer(fTicksPerTickRate, fTickRate);

	do
	{
		UG::ClearScreen();
		UG::Process();
		myTimer->Tick();
	} while (myTimer->GetTime() < fPauseLength);

	delete[] myTimer;
}



void SpaceInvaders::GetScores(int* a_piScoreArray)
{
	std::fstream scoreFileIn;

	scoreFileIn.open(cpScoreFilePath, std::ios_base::in | std::ios_base::binary);
		
	if (scoreFileIn.is_open())
	{
		int count = 0;

		// Read a file to the end
		while (!scoreFileIn.eof())
		{
			scoreFileIn >> a_piScoreArray[count];
			count++;
		}
	}

	scoreFileIn.close();
}



void SpaceInvaders::SetScores(int a_iScore)
{
	bool bNewHighScore = false;
	int iScoreArray[10];


	GetScores(iScoreArray);

	std::sort(std::begin(iScoreArray), std::end(iScoreArray));

	for (int iScoreCompareLoop = 0; iScoreCompareLoop < 10; iScoreCompareLoop++)
	{
		if (iScoreArray[iScoreCompareLoop] < a_iScore)
		{
			bNewHighScore = true;
			break;
		}
	}

	if (bNewHighScore)
	{

		iScoreArray[0] = a_iScore;

		std::sort(std::begin(iScoreArray), std::end(iScoreArray));

		std::fstream scoreFileOut;

		scoreFileOut.open(cpScoreFilePath, std::ios_base::out | std::ios_base::binary);

		if (scoreFileOut.is_open())
		{
			for (int i = 0; i < 10; i++)
			{
				scoreFileOut << iScoreArray[i] << std::endl;
			}

			scoreFileOut.close();
		}
	}
}



SpaceInvaders::GameState SpaceInvaders::GetGameState()
{
	return iGameState;
}



SpaceInvaders::GameMode SpaceInvaders::GetGameMode()
{
	return iGameMode;
}



SpaceInvaders::GameDifficulty SpaceInvaders::GetGameDifficulty()
{
	return iGameDifficulty;
}