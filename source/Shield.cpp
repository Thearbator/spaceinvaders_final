////////////////////////////////////////////////////////////
// File: Shield.cpp
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the Shield class
////////////////////////////////////////////////////////////

#include "UGFW.h"
#include "Shield.h"

Shield::Shield(float a_fX, float a_fY) : c_fWIDTH(88), c_fHEIGHT(64)
{
	iSpriteID = UG::CreateSprite("./images/Barrier/Barrier_1.png", c_fWIDTH, c_fHEIGHT, true);
	Position.x = a_fX;
	Position.y = a_fY;
	iHealth = 20;
	fMoveAmount = 2;
	bDelete = false;

	UG::MoveSprite(iSpriteID, Position.x, Position.y);

	draw();
}

Shield::Shield(const Shield& a_Shield) : c_fWIDTH(88), c_fHEIGHT(64)
{
	iSpriteID = a_Shield.iSpriteID;
	Position.x = a_Shield.Position.x;
	Position.y = a_Shield.Position.y;
	fMoveAmount = a_Shield.fMoveAmount;
	iHealth = a_Shield.iHealth;
	bDelete = a_Shield.bDelete;
}

Shield::~Shield()
{
	UG::StopDrawingSprite(iSpriteID);
	UG::DestroySprite(iSpriteID);
}

void Shield::MoveUp()
{
	Position.y += fMoveAmount;
	UG::MoveSprite(iSpriteID, Position.x, Position.y);
}

void Shield::draw()
{
	UG::DrawSprite(iSpriteID);
}