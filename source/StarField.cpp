////////////////////////////////////////////////////////////
// File: StarField.cpp
// Author: Zack Raeburn
// Date Created: 13/12/2016
// Brief: This file holds the code for the star field class
////////////////////////////////////////////////////////////

#include "UGFW.h"
#include "StarField.h"

#include <random>

StarField::StarField(float a_fScreenWidth, float a_fScreenHeight)
{
	iScreenWidth = (int)a_fScreenWidth;
	iScreenHeight = (int)a_fScreenHeight;
	iStarMoveSpeed = -1;

	iForegroundSize = 3;
	iMidgroundSize = 2;
	iBackgroundSize = 1;

	// Seeding random numbers with the current time
	srand(static_cast<int>(time_t(NULL)));

	srand(rand());

	for (int iStarPosLoop = 0; iStarPosLoop < MAX_STARS; iStarPosLoop++)
	{
		posForeground[iStarPosLoop].x = (float)(rand() % iScreenWidth);
		posForeground[iStarPosLoop].y = (float)(rand() % iScreenHeight);
	}

	for (int iStarPosLoop = 0; iStarPosLoop < MAX_STARS; iStarPosLoop++)
	{
		posMidground[iStarPosLoop].x = (float)(rand() % iScreenWidth);
		posMidground[iStarPosLoop].y = (float)(rand() % iScreenHeight);
	}

	for (int iStarPosLoop = 0; iStarPosLoop < MAX_STARS; iStarPosLoop++)
	{
		posBackground[iStarPosLoop].x = (float)(rand() % iScreenWidth);
		posBackground[iStarPosLoop].y = (float)(rand() % iScreenHeight);
	}
}

void StarField::Update()
{
	for (int iStarUpdateLoop = 0; iStarUpdateLoop < MAX_STARS; iStarUpdateLoop++)
	{
		posBackground[iStarUpdateLoop].x += (float)iStarMoveSpeed * 0.3f;
		posMidground[iStarUpdateLoop].x += (float)iStarMoveSpeed * 0.6f;
		posForeground[iStarUpdateLoop].x += (float)iStarMoveSpeed;

		if (posForeground[iStarUpdateLoop].x < 0.f)
		{
			posForeground[iStarUpdateLoop].x = (float)(iScreenWidth + 1.f);
			posForeground[iStarUpdateLoop].y = (float)(rand() % iScreenHeight);
		}

		if (posMidground[iStarUpdateLoop].x < 0.f)
		{
			posMidground[iStarUpdateLoop].x = (float)(iScreenWidth + 1.f);
			posMidground[iStarUpdateLoop].y = (float)(rand() % iScreenHeight);
		}

		if (posBackground[iStarUpdateLoop].x < 0.f)
		{
			posBackground[iStarUpdateLoop].x = (float)(iScreenWidth + 1.f);
			posBackground[iStarUpdateLoop].y = (float)(rand() % iScreenHeight);
		}

		UG::DrawLine((int)posForeground[iStarUpdateLoop].x - iForegroundSize, (int)posForeground[iStarUpdateLoop].y - iForegroundSize, (int)posForeground[iStarUpdateLoop].x + iForegroundSize, (int)posForeground[iStarUpdateLoop].y + iForegroundSize, UG::SColour(0xff, 0xff, 0xff, 0xff));
		UG::DrawLine((int)posForeground[iStarUpdateLoop].x - iForegroundSize, (int)posForeground[iStarUpdateLoop].y + iForegroundSize, (int)posForeground[iStarUpdateLoop].x + iForegroundSize, (int)posForeground[iStarUpdateLoop].y - iForegroundSize, UG::SColour(0xff, 0xff, 0xff, 0xff));

		UG::DrawLine((int)posMidground[iStarUpdateLoop].x - iMidgroundSize, (int)posMidground[iStarUpdateLoop].y - iMidgroundSize, (int)posMidground[iStarUpdateLoop].x + iMidgroundSize, (int)posMidground[iStarUpdateLoop].y + iMidgroundSize, UG::SColour(0xff, 0xff, 0xff, 0xff));
		UG::DrawLine((int)posMidground[iStarUpdateLoop].x - iMidgroundSize, (int)posMidground[iStarUpdateLoop].y + iMidgroundSize, (int)posMidground[iStarUpdateLoop].x + iMidgroundSize, (int)posMidground[iStarUpdateLoop].y - iMidgroundSize, UG::SColour(0xff, 0xff, 0xff, 0xff));

		UG::DrawLine((int)posBackground[iStarUpdateLoop].x - iBackgroundSize, (int)posBackground[iStarUpdateLoop].y - iBackgroundSize, (int)posBackground[iStarUpdateLoop].x + iBackgroundSize, (int)posBackground[iStarUpdateLoop].y + iBackgroundSize, UG::SColour(0xff, 0xff, 0xff, 0xff));
		UG::DrawLine((int)posBackground[iStarUpdateLoop].x - iBackgroundSize, (int)posBackground[iStarUpdateLoop].y + iBackgroundSize, (int)posBackground[iStarUpdateLoop].x + iBackgroundSize, (int)posBackground[iStarUpdateLoop].y - iBackgroundSize, UG::SColour(0xff, 0xff, 0xff, 0xff));
	}
}