////////////////////////////////////////////////////////////
// File: Alien.cpp
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the Alien class
////////////////////////////////////////////////////////////

#include "Alien.h"
#include "SpaceInvaders.h"

#include <iostream>
#include <random>

// Alien Constructor
Alien::Alien(float a_fXPos, float a_fYPos, AlienType a_AlienType, UG::SColour a_SColour) : c_fALIENSIZE(40), c_fMOVEAMOUNTX((const unsigned int)40), c_fMOVEAMOUNTY((const unsigned int)35)
{
	// Seeding random twice because I like my numbers even more random
	srand(static_cast<int>(time_t(NULL)));
	srand(rand());

	iMoveDirection = 1;

	switch (a_AlienType)
	{
	case Points10:
		{
			iPoints = 10;
			uvCoords[0][0] = 0.03f; uvCoords[0][1] = 0.7f, uvCoords[0][2] = 0.24f, uvCoords[0][3] = 0.82f;
			uvCoords[1][0] = 0.27f; uvCoords[1][1] = 0.7f, uvCoords[1][2] = 0.48f, uvCoords[1][3] = 0.82f;
			break;
		}
	case Points20:
		{
			iPoints = 20;
			uvCoords[0][0] = 0.02f; uvCoords[0][1] = 0.85f, uvCoords[0][2] = 0.27f, uvCoords[0][3] = 1.f;
			uvCoords[1][0] = 0.27f; uvCoords[1][1] = 0.85f, uvCoords[1][2] = 0.52f, uvCoords[1][3] = 1.f;
			break;
		}
	case Points30:
		{
			iPoints = 30;
			uvCoords[0][0] = 0.52f; uvCoords[0][1] = 0.85f, uvCoords[0][2] = 0.72f, uvCoords[0][3] = 1.f;
			uvCoords[1][0] = 0.73f; uvCoords[1][1] = 0.85f, uvCoords[1][2] = 0.95f, uvCoords[1][3] = 1.f;
			break;
		}
	default:
		{
			std::cout << "Error assigning sprite in Alien Class Constructor" << std::endl;
			break;
		}
	}


	// Sets the bullet fire chance, the higher it is the less likely a bullet will be fired
	fBulletFireChance = 50.f;
	fBulletFireChanceIncrease = 0.001f;

	//iSpriteID = UG::CreateSprite(cpTexture1, c_fALIENSIZE, c_fALIENSIZE, true, a_SColour);
	float center[2] = { 0.5, 0.5f };
	float size[2] = { c_fALIENSIZE, c_fALIENSIZE };
	iAnimFrame = 0;
	iSpriteID = UG::CreateSprite("./images/invaders.png", size, center, uvCoords[0], a_SColour);

	Position.x = a_fXPos;
	Position.y = a_fYPos;
	UG::MoveSprite(GetSpriteID(), Position.x, Position.y);
}

Alien::Alien(const Alien& a_Alien) : c_fALIENSIZE(a_Alien.c_fALIENSIZE), c_fMOVEAMOUNTX(a_Alien.c_fMOVEAMOUNTX), c_fMOVEAMOUNTY(a_Alien.c_fMOVEAMOUNTY)
{
	iSpriteID = UG::DuplicateSprite(a_Alien.iSpriteID);
	iMoveDirection = a_Alien.iMoveDirection;
	Position = a_Alien.Position;
	iPoints = a_Alien.iPoints;
	uvCoords[0][0] = a_Alien.uvCoords[0][0];  uvCoords[0][1] = a_Alien.uvCoords[0][1]; uvCoords[0][2] = a_Alien.uvCoords[0][2]; uvCoords[0][3] = a_Alien.uvCoords[0][3];
	uvCoords[1][0] = a_Alien.uvCoords[1][0];  uvCoords[1][1] = a_Alien.uvCoords[1][1]; uvCoords[1][2] = a_Alien.uvCoords[1][2]; uvCoords[1][3] = a_Alien.uvCoords[1][3];

}

// Alien Destructor
Alien::~Alien()
{
	std::cout << "Alien object " << iSpriteID << " destructor called" << std::endl;
	UG::StopDrawingSprite(iSpriteID);
	UG::DestroySprite(iSpriteID);
}

// Getters for Position
float Alien::GetPosX()
{
	return Position.x;
}

float Alien::GetPosY()
{
	return Position.y;
}

float Alien::GetSize()
{
	return c_fALIENSIZE;
}

int Alien::GetSpriteID()
{
	return iSpriteID;
}

int Alien::GetPoints()
{
	return iPoints;
}

short int Alien::SpriteInScreen(float a_fScreenWidth)
{
	if ((int)Position.x < (int)(0.f + (c_fALIENSIZE * 2)))
	{
		return -1;
	}
	else if ((int)Position.x > (int)(a_fScreenWidth - (c_fALIENSIZE * 2)))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

short int Alien::GetMoveDirection()
{
	return iMoveDirection;
}

// Need a custom destructor so when I create objects via a loop they don't get destroyed by the normal destructor
void Alien::DestroyObject()
{
	UG::StopDrawingSprite(iSpriteID);
	UG::DestroySprite(iSpriteID);
}

// Setter for Position
void Alien::SetPos(float a_fXPos, float a_fYPos)
{
	Position.x = a_fXPos;
	Position.y = a_fYPos;
	UG::MoveSprite(iSpriteID, Position.x, Position.y);
}

// To move aliens
void Alien::UpdateAlien(std::vector<Bullet*>& a_vrBulletVector, float a_fScreenWidth)
{
	SwitchSprite();
	SetPos(Position.x + ((float)iMoveDirection * (float)c_fMOVEAMOUNTX), Position.y);

	if ((rand() % (int)fBulletFireChance) == 0)
	{
		FireBullet(a_vrBulletVector, a_fScreenWidth);
	}

	// Makes sure that the aliens don't turn into machine guns
	// I didn't think it was worth making a variable here for this number as it is not all that important
	if (fBulletFireChance > 5)
	{
		fBulletFireChance -= fBulletFireChanceIncrease;
	}
}

void Alien::MoveDown()
{
	SwitchSprite();
	iMoveDirection = -iMoveDirection;
	SetPos(Position.x, Position.y - c_fMOVEAMOUNTY);
}

void Alien::SwitchSprite()
{
	++iAnimFrame;
	iAnimFrame = iAnimFrame % 2;
	UG::SetSpriteUVCoordinates(iSpriteID, uvCoords[iAnimFrame]);

}

void Alien::Shot()
{

	UG::StopDrawingSprite(iSpriteID);
	UG::DestroySprite(iSpriteID);

}

void Alien::FireBullet(std::vector<Bullet*> &a_vrBulletVector, float fScreenHeight)
{

	// Creates an instance of a bullet and adds it to the vector
	Bullet *alienBullet = new Bullet(Position.x, Position.y - (c_fALIENSIZE/2), (float)fScreenHeight, Bullet::BulletType::Alien);
	a_vrBulletVector.push_back(alienBullet);

}