////////////////////////////////////////////////////////////
// File: TitleText.cpp
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the title text class
////////////////////////////////////////////////////////////

#include "TitleText.h"

#include <string>

TitleText::TitleText(std::string a_sText, float a_fXPos, float a_fYPos, bool a_bDraw, UG::SColour a_Colour)
	{
		Position.x = a_fXPos;
		Position.y = a_fYPos;
		sText = a_sText;
		bDraw = a_bDraw;
		colour = a_Colour;
	}