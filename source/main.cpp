////////////////////////////////////////////////////////////
// File: main.cpp
// Author: Zack Raeburn
// Date Created: 10/11/2016
// Brief: The source file for the main program code
////////////////////////////////////////////////////////////

#include "Main.h"

int main(int argv, char* argc[])
{
	// Numbers that control the window size and Position
	float fScreenWidth = 1024.f, fScreenHeight = 768.f;
	int iScreenStartPosition = 100;

	if (UG::Create((int)fScreenWidth, (int)fScreenHeight, false, "Space Invaders", iScreenStartPosition, iScreenStartPosition))
	{		
#pragma region Initialisation of game objects and variables

		// Makes the background of the screen black before anything else if rendered in front of it
		UG::SetBackgroundColor(UG::SColour(0, 0, 0, 0xFF));
		UG::AddFont("./fonts/invaders.fnt");

		SpaceInvaders mySpaceInvaders(fScreenWidth, fScreenHeight);	

#pragma endregion

		do
		{
			UG::ClearScreen();

			// Changes what block of code to execute based of the current game state enum
			switch (mySpaceInvaders.GetGameState())
			{
				case Start:
					{
						mySpaceInvaders.GameStartSplashScreen();
						break;
					}
				case Menu:
					{
						mySpaceInvaders.GameMenuScreen();
						break;
					}
				case HighScores:
					{
						mySpaceInvaders.GameHighScoreScreen();
						break;
					}
				case DifficultySelect:
					{
						mySpaceInvaders.GameDifficultySelect();
						break;
					}
				case Game:
					{
						mySpaceInvaders.PlayGame();
						break;
					}
				case GameOver:
					{
						mySpaceInvaders.GameOverScreen();
						break;
					}
				default:
					{
						break;
					}
			}

		} while (UG::Process());

		UG::Dispose();
	}
	return 0;
}