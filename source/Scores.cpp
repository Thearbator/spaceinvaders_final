////////////////////////////////////////////////////////////
// File: Scores.cpp
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the Scores class
////////////////////////////////////////////////////////////

#include "Scores.h"

Scores::Scores(const float a_ScoreTextXPos, const float a_fScoreTextYPos, float a_fScore)

{
	Position.x = a_ScoreTextXPos;
	Position.y = a_fScoreTextYPos;
	fScore = a_fScore;
}

Scores Scores::operator= (const Scores& a_Score)
{
	Position.x = a_Score.Position.x;
	Position.y = a_Score.Position.y;
	fScore = a_Score.fScore;

	return *this;
}