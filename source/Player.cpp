////////////////////////////////////////////////////////////
// File: Player.cpp
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the player class
////////////////////////////////////////////////////////////

#include "Player.h"
#include "UGFW.h"
#include "Enumerations.h"

#include <iostream>

// Player class constructors
Player::Player(float a_fPlayerPosX, float a_fPlayerPosY, bool a_bDraw) : c_fPlayerWidth(60), c_fPlayerHeight(36)
{
	iSpriteID = UG::CreateSprite("./images/Tank/Tank.png", c_fPlayerWidth, c_fPlayerHeight, true);

	bDelete = false;

	Position.x = a_fPlayerPosX;
	Position.y = a_fPlayerPosY;
	UG::MoveSprite(GetSpriteID(), Position.x, Position.y);

	if (a_bDraw)
	{
		draw();
	}
}

Player::Player(float a_fPlayerPosX, float a_fPlayerPosY, SpaceInvaders::GameDifficulty a_GameDifficulty, bool a_bDraw) : c_fPlayerWidth(60), c_fPlayerHeight(36)
{
	iSpriteID = UG::CreateSprite("./images/Tank/Tank.png", c_fPlayerWidth, c_fPlayerHeight, true);

	iScreenWidth = 1024;
	iScreenHeight = 768;

	switch (a_GameDifficulty)
	{
	case SpaceInvaders::GameDifficulty::Easy:
		{
			siLives = 3;
			iMaxBullets = 3;
			fPlayerMoveSpeed = 6.f;
			break;
		}
	case SpaceInvaders::GameDifficulty::Medium:
		{
			siLives = 2;
			iMaxBullets = 2;
			fPlayerMoveSpeed = 5.f;
			break;
		}
	case SpaceInvaders::GameDifficulty::Hard:
		{
			siLives = 1;
			iMaxBullets = 1;
			fPlayerMoveSpeed = 4.f;
			break;
		}
	default:
		{
			siLives = 3;
			iMaxBullets = 3;
			fPlayerMoveSpeed = 10.f;
			break;
		}
	}

	uiPlayerMoveLeftKey = UG::KEY_LEFT;
	uiPlayerMoveRightKey = UG::KEY_RIGHT;
	uiPlayerShootKey = UG::KEY_SPACE;

	Position.x = a_fPlayerPosX;
	Position.y = a_fPlayerPosY;
	UG::MoveSprite(GetSpriteID(), Position.x, Position.y);

	if (a_bDraw)
	{
		draw();
	}
}

// Player class destructor
Player::~Player()
{
	UG::StopDrawingSprite(GetSpriteID());
	UG::DestroySprite(iSpriteID);
}

// Gets Position x
float Player::GetPosX()
{
	return Position.x;
}

// GEts Position Y
float Player::GetPosY()
{
	return Position.y;
}

// Gets the player sprite ID
int Player::GetSpriteID()
{
	return iSpriteID;
}

short int Player::GetLives()
{
	return siLives;
}

unsigned int Player::GetMoveLeftKey()
{
	return uiPlayerMoveLeftKey;
}

float Player::GetWidth()
{
	return c_fPlayerWidth;
}

float Player::GetHeight()
{
	return c_fPlayerHeight;
}

unsigned int Player::GetMoveRightKey()
{
	return uiPlayerMoveRightKey;
}

void Player::draw()
{
	std::cout << "Player " << iSpriteID << std::endl;
	UG::DrawSprite(iSpriteID);
}

// For moving the player and receiving inputs
void Player::update(std::vector<Bullet*> &a_vrPlayerBulletVector)
{
	bool canMoveLeft = true, canMoveRight = true;
	short int iMoveSprite = 0;

	// Checks for inputs from the movement keys specified in the class definition
	if (UG::IsKeyDown(uiPlayerMoveRightKey))
	{
		iMoveSprite++;
	}
	if (UG::IsKeyDown(uiPlayerMoveLeftKey))
	{
		iMoveSprite--;
	}

	if (UG::IsKeyDown(uiPlayerShootKey))
	{
		FireBullet(a_vrPlayerBulletVector);
	}

	// Checks if the player can move (if the player is on the screen)
	if (Position.x + (c_fPlayerWidth / 2) >= (float)iScreenWidth)
	{
		canMoveRight = false;
	}
	if (Position.x - (c_fPlayerWidth / 2) <= 0)
	{
		canMoveLeft = false;
	}

	// Moves the player if it can be moved
	if (iMoveSprite > 0 && canMoveRight == true)
	{
		Position.x += fPlayerMoveSpeed;
		UG::MoveSprite(GetSpriteID(), Position.x, Position.y);
	}
	if (iMoveSprite < 0 && canMoveLeft == true)
	{
		Position.x -= fPlayerMoveSpeed;
		UG::MoveSprite(GetSpriteID(), Position.x, Position.y);
	}

	for (int iBulletUpdater = 0; iBulletUpdater < a_vrPlayerBulletVector.size(); iBulletUpdater++)
	{
		if (a_vrPlayerBulletVector[iBulletUpdater]->SpriteOnScreen() == false)
		{
			Bullet* bullet = *(a_vrPlayerBulletVector.begin() + iBulletUpdater);
			delete bullet;

			a_vrPlayerBulletVector.erase(a_vrPlayerBulletVector.begin() + iBulletUpdater);
		}
	}

}

// Creates an instance of PlayerBullet and puts it into a vector
void Player::FireBullet(std::vector<Bullet*> &a_vrPlayerBulletVector)
{
	int iAmountOfPlayerBullets = 0;

	for (int iBulletTypeChecker = 0; iBulletTypeChecker < a_vrPlayerBulletVector.size(); iBulletTypeChecker++)
	{
		if (a_vrPlayerBulletVector[iBulletTypeChecker]->thisBulletType == Bullet::BulletType::Player)
		{
			iAmountOfPlayerBullets++;
		}
	}

	// Checks if there are already bullets fired
	if (iAmountOfPlayerBullets < iMaxBullets)
	{
		// Creates an instance of a bullet and adds it to the vector
		Bullet *playerBullet = new Bullet(Position.x, Position.y + (c_fPlayerHeight/2) + 10, (float)iScreenWidth);
		a_vrPlayerBulletVector.push_back(playerBullet);
	}
}

void Player::SetLives(short int a_siLives)
{
	siLives = a_siLives;
}

void Player::SetPos(float a_fX, float a_fY)
{
	Position.x = a_fX;
	Position.y = a_fY;
	UG::MoveSprite(iSpriteID, Position.x, Position.y);
}

void Player::ResetPlayer()
{

}