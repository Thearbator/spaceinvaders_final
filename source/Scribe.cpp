#define _CRT_SECURE_NO_WARNINGS

#include "Scribe.h"

#include <Windows.h>
#include <iostream>


typedef struct fHeader_struct
{
	char fID[12];
	uint16_t version = 1;
	uint16_t numFiles = 0;
}fHeader;



typedef struct fData_struct
{
	char fileName[256];
	uint16_t fileSize;
}fData;



Scribe::Scribe()
{
	
}



void Scribe::createDataFile(const char* binPath, const char* dirPath)
{
	fHeader fileHeader;
	std::vector<std::string> files;

	getFileNames(files, dirPath);

	fileHeader.numFiles = (uint16_t)files.size();
	
	std::ofstream binaryFile(binPath, std::ios::binary);
	binaryFile.write((char*)&fileHeader, sizeof(fHeader));

	for (std::string file : files)
	{

		int sizeOfFullPath = strlen(dirPath) + strlen(file.c_str()) + 2;
		char* fullPath = new char[sizeOfFullPath];

		strcpy(fullPath, dirPath);
		strcpy(fullPath, "\\");
		strcpy(fullPath, file.c_str());
		
		std::ifstream fileTobePacked(fullPath, std::ifstream::ate | std::ifstream::binary);
		int fileSize = (int)fileTobePacked.tellg();
		fileTobePacked.seekg(std::ifstream::beg);

		fData fileData;
		strcpy(fileData.fileName, file.c_str());
		fileData.fileSize = fileSize;

		std::cout << "Packing" << file << std::endl;

		binaryFile.write((char*)&fileData, sizeof(fData));

		char* memoryBuffer = new char[fileSize];
		fileTobePacked.read(memoryBuffer, fileSize);
		binaryFile.write(memoryBuffer, fileSize);

		delete[] memoryBuffer;
		delete[] fullPath;
		fileTobePacked.close();

	}

	binaryFile.close();

}



void Scribe::getFileNames(std::vector<std::string>& output, const char* path)
{
	https://msdn.microsoft.com/en-us/library/windows/desktop/aa383751(v=vs.85).aspx
	WIN32_FIND_DATAA foundFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	LPSTR fullpath = new char[MAX_PATH];

	//Gets the full path name of the supplied path
	//https://msdn.microsoft.com/en-us/library/windows/desktop/aa364963(v=vs.85).aspx
	GetFullPathNameA(path, MAX_PATH, fullpath, nullptr);

	//Set up a char array sized for our path
	char *fullPath = new char[strlen(path) + 3];
	strcpy(fullPath, path);
	strcat(fullPath, "\\*");

	//Use that char array, and store results in foundFileData
	//https://msdn.microsoft.com/en-us/library/windows/desktop/aa364418(v=vs.85).aspx
	hFind = FindFirstFileA(fullPath, &foundFileData);

	//Delete (free up) the memory used for our character array
	delete[] fullPath;

	//If our find handle is valid, search through it for files
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if ((foundFileData.cFileName[0] != '.'))
			{
				output.push_back(foundFileData.cFileName);
			}
			//https://msdn.microsoft.com/en-us/library/windows/desktop/aa364428(v=vs.85).aspx
		} while (FindNextFileA(hFind, &foundFileData) != 0);
	}

	//Close our find handle
	FindClose(hFind);
}



void Scribe::extractFile(const char* fileName, const char* searcj, const char* extractPath)
{



}