////////////////////////////////////////////////////////////
// File: Bullet.cpp
// Author: Zack Raeburn
// Date Created: 05/12/2016
// Brief: A source file for the bullet class
////////////////////////////////////////////////////////////

#include "Bullet.h"
#include "UGFW.h"

#include <iostream>

// Alien class constructor
Bullet::Bullet(float a_fPosX, float a_fPosY, const float a_c_fSCREENHEIGHT, BulletType a_ThisBulletType, bool a_bDelete) : c_fBULLETWIDTH(4.f), c_fBULLETHEIGHT(24.f), c_fSCREENHEIGHT(a_c_fSCREENHEIGHT)
{
	Position.x = a_fPosX;
	Position.y = a_fPosY;

	thisBulletType = a_ThisBulletType;

	bDelete = a_bDelete;

	if (a_ThisBulletType == Player)
	{
		iSpriteID = UG::CreateSprite("./images/Missile/Missile_1.png", c_fBULLETWIDTH, c_fBULLETHEIGHT, true, UG::SColour(0, 0xff, 0, 0xff));
		fBulletSpeed = 8;
	}
	else if (a_ThisBulletType == Alien)
	{
		fBulletSpeed = -fBulletSpeed;
		iSpriteID = UG::CreateSprite("./images/Missile/Missile_2.png", c_fBULLETWIDTH, c_fBULLETHEIGHT, true);
		fBulletSpeed = -4;
	}
	
	UG::MoveSprite(GetSpriteID(), Position.x, Position.y);
	draw();
}

// Alien class destructor
Bullet::~Bullet()
{
	std::cout << "Bullet destroyed" << std::endl;
	UG::StopDrawingSprite(iSpriteID);
	UG::DestroySprite(GetSpriteID());
}

// Returns the sprite ID
int Bullet::GetSpriteID()
{
	return iSpriteID;
}

bool Bullet::SpriteOnScreen()
{
	if ((Position.y + c_fBULLETHEIGHT) < c_fSCREENHEIGHT || Position.y - c_fBULLETHEIGHT < 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

float Bullet::GetWidth()
{
	return c_fBULLETWIDTH;
}

float Bullet::GetHeight()
{
	return c_fBULLETHEIGHT;
}

float Bullet::GetPosX()
{
	return Position.x;
}

float Bullet::GetPosY()
{
	return Position.y;
}

void Bullet::UpdateBullet()
{
	Position.y += fBulletSpeed;
	UG::MoveSprite(GetSpriteID(), Position.x, Position.y);
}

void Bullet::draw()
{
	UG::DrawSprite(GetSpriteID());
}